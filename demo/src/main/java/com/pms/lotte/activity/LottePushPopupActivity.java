package com.pms.lotte.activity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebSettings.PluginState;
import android.webkit.WebSettings.RenderPriority;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.FrameLayout;
import android.widget.FrameLayout.LayoutParams;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.ImageLoader.ImageContainer;
import com.android.volley.toolbox.ImageLoader.ImageListener;
import com.android.volley.toolbox.Volley;

import com.pms.demo.R;
import com.pms.sdk.IPMSConsts;
import com.pms.sdk.bean.Msg;
import com.pms.sdk.bean.PushMsg;
import com.pms.sdk.common.util.CLog;
import com.pms.sdk.common.util.PMSUtil;
import com.pms.sdk.common.util.Prefs;
import com.pms.sdk.push.PMSWebViewBridge;
import com.pms.sdk.push.PushReceiver;
import com.pms.sdk.view.BitmapLruCache;

/**
 * push popup activity
 * 
 * @author erzisk
 * @since 2013.06.07
 */
public class LottePushPopupActivity extends Activity implements IPMSConsts {

	private static final String TAG = "[" + LottePushPopupActivity.class.getSimpleName() + "]";

	private Context mContext;

	private FrameLayout mLayBack;
	private FrameLayout mLayOuter;
	private LayoutInflater inflate;

	private WebView mWv;
	private ImageView mImg;

	private PushMsg pushMsg;

	private int mShowingTime;

	private final Thread mThread = new Thread(new Runnable() {
		@Override
		public void run () {
			while (mShowingTime > -1) {
				mShowingTime -= 1000;
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
			finish();
		}
	});

	@SuppressLint("InlinedApi")
	@Override
	protected void onCreate (Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().addFlags(WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED);
		inflate = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);

		mContext = this;
		// set view
		mLayBack = new FrameLayout(this);
		mLayOuter = new FrameLayout(this);

		mLayBack.setLayoutParams(new LayoutParams(-1, -1));
		mLayBack.setPadding(50, 50, 50, 50);
		LayoutParams lp = new LayoutParams(-1, -2);
		lp.gravity = Gravity.CENTER;
		mLayOuter.setLayoutParams(lp);

		mLayBack.addView(mLayOuter);
		setContentView(mLayBack);

		setPushPopup(getIntent());
	}

	@SuppressLint("NewApi")
	@Override
	protected void onResume () {
		try {
			WebView.class.getMethod("onResume").invoke(mWv);
			mWv.resumeTimers();
		} catch (Exception e) {
		}
		super.onResume();
		overridePendingTransition(0, 0);
	}

	@Override
	protected void onNewIntent (Intent intent) {
		super.onNewIntent(intent);
		setPushPopup(intent);
	}

	/**
	 * set rich popup
	 * 
	 * @param intent
	 */
	private void setPushPopup (Intent intent) {
		pushMsg = new PushMsg(intent.getExtras());

		CLog.i(TAG + pushMsg);
		mLayOuter.removeAllViews();

		if (Msg.TYPE_H.equals(pushMsg.msgType) || Msg.TYPE_L.equals(pushMsg.msgType)) {
			mLayOuter.addView(getRichPopup(mContext, pushMsg.message));
		} else {
			mLayOuter.addView(getTextPopup(mContext, pushMsg.notiMsg));
		}

		Prefs pref = new Prefs(this);
		mShowingTime = pref.getInt(PREF_PUSH_POPUP_SHOWING_TIME);
		if (!mThread.isAlive()) {
			mThread.start();
		}
	}

	/**
	 * get rich popup
	 * 
	 * @param context
	 * @param url
	 * @return
	 */
	@SuppressLint({ "NewApi", "SetJavaScriptEnabled" })
	@SuppressWarnings("deprecation")
	private View getRichPopup (Context context, final String url) {
		CLog.i(TAG + "getRichPopup");

		View view = inflate.inflate(R.layout.pms_rich_popup, null);

		ImageView mCloseBtn = (ImageView) view.findViewById(R.id.pms_rich_close);
		// 닫기 버튼 영역 클릭에 대한 이벤트 처리
		mCloseBtn.setOnClickListener(onClickListener);

		final ProgressBar mPrg = (ProgressBar) view.findViewById(R.id.pms_rich_prg);

		mWv = (WebView) view.findViewById(R.id.pms_rich_wv);
		mImg = (ImageView) view.findViewById(R.id.pms_rich_img);

		if (Msg.TYPE_L.equals(pushMsg.msgType) && url.startsWith("http") && (url.endsWith(".png") || url.endsWith(".jpg") || url.endsWith(".gif"))) {
			mPrg.setVisibility(View.GONE);
			mWv.setVisibility(View.GONE);
			mImg.setVisibility(View.VISIBLE);

			RequestQueue queue = Volley.newRequestQueue(context);
			ImageLoader imageLoader = new ImageLoader(queue, new BitmapLruCache());

			imageLoader.get(url, new ImageListener() {
				@Override
				public void onResponse (ImageContainer response, boolean isImmediate) {
					if (response != null && response.getBitmap() != null) {
						mImg.setImageBitmap(response.getBitmap());
					}
				}

				@Override
				public void onErrorResponse (VolleyError error) {
					CLog.e(TAG + "onErrorResponse:" + error.getMessage());
				}
			});

			mImg.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick (View v) {
					startNotiReceiver();
					finish();
				}
			});

			// image경우 push로 넘어온 app_link로 보냄.
			mWv.setOnTouchListener(onTouchImageListener);

		} else {
			mPrg.setVisibility(View.VISIBLE);
			mWv.setVisibility(View.VISIBLE);
			mImg.setVisibility(View.GONE);

			mWv.clearCache(true);
			mWv.clearHistory();
			mWv.clearFormData();

			mWv.setInitialScale(1);
			mWv.setBackgroundColor(android.R.style.Theme_Translucent);
			mWv.setHorizontalScrollBarEnabled(false);
			mWv.setVerticalScrollBarEnabled(false);

			WebSettings settings = mWv.getSettings();
			settings.setJavaScriptEnabled(true);
			settings.setJavaScriptCanOpenWindowsAutomatically(true);
			settings.setSupportMultipleWindows(true);
			settings.setUseWideViewPort(true);
			settings.setLoadWithOverviewMode(true);
			settings.setPluginState(PluginState.ON);
			settings.setRenderPriority(RenderPriority.HIGH);
			settings.setCacheMode(WebSettings.LOAD_NO_CACHE);
			settings.setGeolocationEnabled(true);
			settings.setDomStorageEnabled(true);
			settings.setUseWideViewPort(true);

			mWv.setWebChromeClient(new WebChromeClient() {
				@Override
				public void onProgressChanged (WebView view, int newProgress) {
					super.onProgressChanged(view, newProgress);
					mPrg.setProgress(newProgress);
					if (newProgress >= 100) {
						mPrg.setVisibility(View.GONE);
					}
				}
			});

			PMSWebViewBridge pmsBridge = new PMSWebViewBridge(mContext);
			mWv.addJavascriptInterface(pmsBridge, "pms");

			// setting html to webview
			if (Msg.TYPE_L.equals(pushMsg.msgType) && url.startsWith("http")) {
				mWv.loadUrl(url);
			} else {
				mWv.loadData(url, "text/html; charset=utf-8", null);
			}

			mWv.setOnTouchListener(onTouchLinkListener);
		}

		mWv.setWebViewClient(new WebViewClient() {
			@Override
			public boolean shouldOverrideUrlLoading (WebView view, String url) {
				CLog.i(TAG + "webViewClient:url=" + url);
				return super.shouldOverrideUrlLoading(view, url);
			}
		});

		return view;
	}

	/**
	 * get Text popup
	 * 
	 * @param context
	 * @param url
	 * @return
	 */
	private View getTextPopup (Context context, String msg) {
		CLog.i(TAG + "getTextPopup");

		View view = inflate.inflate(R.layout.pms_text_popup, null);
		ImageView positiveButton = (ImageView) view.findViewById(R.id.pms_txt_detail);
		ImageView negativeButton = (ImageView) view.findViewById(R.id.pms_txt_close);

		((TextView) view.findViewById(R.id.pms_txt_title)).setText(pushMsg.notiTitle);
		((TextView) view.findViewById(R.id.pms_txt_msg)).setText(msg);

		positiveButton.setOnClickListener(onClickListener);
		negativeButton.setOnClickListener(onClickListener);

		return view;
	}

	/**
	 * onClick Rich
	 */
	private final OnClickListener onClickListener = new OnClickListener() {
		@Override
		public void onClick (View v) {

			int id = v.getId();
			if (id == R.id.pms_txt_detail) {
				startNotiReceiver();
			} else {
				// read msg
			}

			finish();
		}
	};

	/**
	 * onTouchListener
	 */
	private float touchX, touchY;
	private final OnTouchListener onTouchImageListener = new OnTouchListener() {
		@Override
		public boolean onTouch (View v, MotionEvent event) {
			if (event.getAction() == MotionEvent.ACTION_DOWN) {
				touchX = event.getX();
				touchY = event.getY();
			} else if (event.getAction() == MotionEvent.ACTION_UP) {
				if (Math.abs(touchX - event.getX()) < 10 && Math.abs(touchY - event.getY()) < 10) {
					startNotiReceiver();
					finish();
				}
			}
			return false;
		}
	};

	/**
	 * on touch link listner
	 */
	private final OnTouchListener onTouchLinkListener = new OnTouchListener() {
		@Override
		public boolean onTouch (View v, MotionEvent event) {
			if (event.getAction() != MotionEvent.ACTION_MOVE) {
				((WebView) v).setWebViewClient(new WebViewClient() {
					@Override
					public boolean shouldOverrideUrlLoading (WebView view, String url) {
						PMSUtil.arrayToPrefs(mContext, PREF_READ_LIST, pushMsg.msgId);
						CLog.i("webViewClient:url=" + url);
						Intent intent = new Intent(Intent.ACTION_VIEW);
						Uri uri = Uri.parse(url);
						intent.setData(uri);
						startActivity(intent);
						// mWv.loadUrl(url);
						return true;
					}
				});
			}
			return false;
		}
	};

	/**
	 * noti receiver
	 */
	private void startNotiReceiver () {
		Prefs prefs = new Prefs(mContext);
		String receiverClass = prefs.getString(PREF_NOTI_RECEIVER);
		receiverClass = receiverClass != null ? receiverClass : "com.pms.sdk.notification";

		Intent intent = new Intent(receiverClass);
		intent.putExtras(getIntent().getExtras());
		mContext.sendBroadcast(intent);

		hideNotifiaction();
	}

	/**
	 * hide notification
	 */
	private void hideNotifiaction () {
		NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
		notificationManager.cancel(PushReceiver.NOTIFICATION_ID);
	}

	@SuppressLint("NewApi")
	@Override
	public void finish () {
		super.finish();
		overridePendingTransition(0, 0);
	}
}
