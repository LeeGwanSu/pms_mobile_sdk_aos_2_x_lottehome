package com.pms.lotte.push;

import org.json.JSONArray;
import org.json.JSONObject;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.pms.sdk.IPMSConsts;
import com.pms.sdk.api.APIManager.APICallback;
import com.pms.sdk.api.request.ReadMsg;
import com.pms.sdk.bean.PushMsg;
import com.pms.sdk.common.util.CLog;
import com.pms.sdk.common.util.PMSUtil;

public class PushNotiReceiver extends BroadcastReceiver implements IPMSConsts {

	private static final String TAG = "[" + PushNotiReceiver.class.getName() + "]";

	@Override
	public void onReceive (Context context, Intent intent) {

		PushMsg pushMsg = new PushMsg(intent.getExtras());

		CLog.i(TAG + pushMsg);

		String appLink;
		try {
			appLink = new JSONObject(pushMsg.data).getString("l");
		} catch (Exception e) {
			appLink = "";
		}

		JSONArray reads = new JSONArray();
		reads.put(pushMsg.msgId);

		new ReadMsg(context).request(null, reads, new APICallback() {
			@Override
			public void response (String arg0, JSONObject arg1) {
				CLog.d("readMsg: " + arg0);
			}
		});

		PMSUtil.arrayToPrefs(context, PREF_READ_LIST, pushMsg.msgId);

		try {
			CLog.d(TAG + "appLink:" + appLink);
			Intent httpIntent = new Intent("com.pms.lotte.push.applink");
			httpIntent.putExtra("l", appLink);
			httpIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			context.sendBroadcast(httpIntent);
			// context.startActivity(httpIntent);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
}
