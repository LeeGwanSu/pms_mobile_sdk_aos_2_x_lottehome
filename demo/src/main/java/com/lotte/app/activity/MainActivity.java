package com.lotte.app.activity;

import com.pms.demo.R;
import org.json.JSONObject;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;


import com.pms.sdk.PMS;
import com.pms.sdk.api.APIManager.APICallback;
import com.pms.sdk.api.request.DeviceCert;
import com.pms.sdk.api.request.LoginPms;
import com.pms.sdk.api.request.LogoutPms;
import com.pms.sdk.api.request.NewMsg;
import com.pms.sdk.api.request.SetConfig;
import com.pms.sdk.common.util.PhoneState;
import com.pms.sdk.common.util.Prefs;

public class MainActivity extends Activity {

	private PMS pms;
	private Context mContext;

	private EditText mEdtCustId;
	private Button mBtnDeviceCert;
	private Button mBtnLoginPms;
	private Button mBtnLogoutPms;
	private Button mBtnNewMsg;
	private Button mBtnSetConfig;
	private Button mBtnMktFlag;
	private Button mBtnInfoFlag;
	private TextView mTxtResult;

	private Boolean mbMktFlag = false;
	private Boolean mbInfoFlag = false;

	@Override
	protected void onCreate (Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.mContext = this;

		this.setContentView(R.layout.main_activity);
		this.mEdtCustId = (EditText) findViewById(R.id.edt_cust_id);
		this.mBtnDeviceCert = (Button) findViewById(R.id.btn_device_cert);
		this.mBtnLoginPms = (Button) findViewById(R.id.btn_login_pms);
		this.mBtnLogoutPms = (Button) findViewById(R.id.btn_logout_pms);
		this.mBtnNewMsg = (Button) findViewById(R.id.btn_new_msg);
		this.mBtnSetConfig = (Button) findViewById(R.id.btn_set_Config);
		this.mBtnMktFlag = (Button) findViewById(R.id.btn_mkt_flag);
		this.mBtnInfoFlag = (Button) findViewById(R.id.btn_info_flag);
		this.mTxtResult = (TextView) findViewById(R.id.txt_result);

		mBtnDeviceCert.setOnClickListener(onClickListener);
		mBtnLoginPms.setOnClickListener(onClickListener);
		mBtnLogoutPms.setOnClickListener(onClickListener);
		mBtnNewMsg.setOnClickListener(onClickListener);
		mBtnSetConfig.setOnClickListener(onClickListener);
		mBtnMktFlag.setOnClickListener(onClickListener);
		mBtnInfoFlag.setOnClickListener(onClickListener);

		// pms 기본 셋팅입니다.
		this.pms = PMS.getInstance(mContext, "997255892867");
		// this.pms.setNotiIcon(R.drawable.ic_launcher);
		this.pms.setNotiReceiver("com.pms.lotte.sdk.notification");
		this.pms.setPushPopupActivity("com.pms.lotte.activity.LottePushPopupActivity");
		this.pms.isGCMPush(true);
		this.pms.setIsPopupActivity(true);
		this.pms.setDebugTAG("Lotte");
		this.pms.setDebugMode(true);
		this.pms.setRingMode(true);
		this.pms.setVibeMode(true);
		this.pms.setScreenWakeup(true);
		this.pms.setPopupNoti(true);
		this.pms.setIsPopupActivity(true);
//		this.pms.startMQTTService(mContext);
		this.pms.setUuid(PhoneState.getGlobalDeviceToken(mContext));
		registerReceiver(receiver, new IntentFilter("com.pms.lotte.push.applink"));

		mEdtCustId.setText(this.pms.getCustId());
		pms.setNotificationStackable(true);
		pms.setUseBigText(true);
	}

	@Override
	protected void onDestroy () {
		super.onDestroy();
		unregisterReceiver(receiver);
		PMS.clear();
	};

	private final BroadcastReceiver receiver = new BroadcastReceiver() {

		@Override
		public void onReceive (Context context, Intent intent) {
			System.out.println(intent.getStringExtra("l"));
		}
	};

	// private void broadcastReceivedMessage (String message) {
	// CLog.d("broadcastReceivedMessage:message = " + message);
	//
	// Intent broadcastIntent = new Intent(IPMSConsts.ACTION_PUSH_RECEIVE);
	// broadcastIntent.addCategory(getApplication().getPackageName());
	//
	// broadcastIntent.putExtra(IPMSConsts.POLLING_KEY, message);
	// sendBroadcast(broadcastIntent);
	// }

	private final OnClickListener onClickListener = new OnClickListener() {

		@Override
		public void onClick (View v) {
			int id = v.getId();
			mTxtResult.setText("loading...");
			if (id == R.id.btn_device_cert) {
				pms.setCustId(mEdtCustId.getText().toString());
				/**
				 * deviceCert (앱이 실행 되는 시점에서 호출 해주시면 됩니다.) 첫번째 파라미터는 CRM데이터 연동을 위한 파라미터로 null로 념겨주시면 됩니다.
				 */
				new DeviceCert(mContext).request(null, new APICallback() {
					@Override
					public void response (String arg0, JSONObject arg1) {
						mTxtResult.setText(arg1.toString());
						// mContext.startService(new Intent(mContext, pollingService.class));
					}
				});
			} else if (id == R.id.btn_login_pms) {
				String strCustID = mEdtCustId.getText().toString();
				strCustID = strCustID == null ? "" : strCustID;
//				Toast.makeText(mContext, "ID는 다음과 같다 : " + strCustID, Toast.LENGTH_SHORT).show();
				new LoginPms(mContext).request(strCustID, null, new APICallback() {
					@Override
					public void response (String code, JSONObject json) {
						String strResponse = "response code : " + code + " / json : " + json.toString();
						Toast.makeText(mContext, strResponse, Toast.LENGTH_SHORT).show();
						mTxtResult.setText(strResponse);
					}
				});
			} else if (id == R.id.btn_logout_pms) {
				new LogoutPms(mContext).request(new APICallback() {
					@Override
					public void response (String code, JSONObject json) {
						mTxtResult.setText(json.toString());
					}
				});
			} else if (id == R.id.btn_set_Config) {
				if (pms.getMsgFlag().equals("N")) {
					new SetConfig(mContext).request(pms.getMsgFlag(), pms.getNotiFlag(), new APICallback() {
						@Override
						public void response (String arg0, JSONObject arg1) {
							mTxtResult.setText(arg1.toString());
						}
					});
				} else {
					new SetConfig(mContext).request(pms.getMsgFlag(), pms.getNotiFlag(), new APICallback() {
						@Override
						public void response (String arg0, JSONObject arg1) {
							mTxtResult.setText(arg1.toString());
						}
					});
				}
			} else if (id == R.id.btn_mkt_flag) {
				Toast.makeText(mContext, "MktFlag 삭제된 버전이지롱", Toast.LENGTH_SHORT).show();
//				if (mbMktFlag == false) {
//					new SetConfig(mContext).request(pms.getMsgFlag(), pms.getNotiFlag(), "Y", new APICallback() {
//						@Override
//						public void response (String arg0, JSONObject arg1) {
//							mTxtResult.setText(arg1.toString());
//							mbMktFlag = true;
//						}
//					});
//				} else {
//					new SetConfig(mContext).request(pms.getMsgFlag(), pms.getNotiFlag(), "N", new APICallback() {
//						@Override
//						public void response (String arg0, JSONObject arg1) {
//							mTxtResult.setText(arg1.toString());
//							mbMktFlag = false;
//						}
//					});
//				}
			} else if (id == R.id.btn_info_flag) {
				if (mbInfoFlag == false) {
					new SetConfig(mContext).request(pms.getMsgFlag(), pms.getNotiFlag(), new APICallback() {
						@Override
						public void response (String arg0, JSONObject arg1) {
							mTxtResult.setText(arg1.toString());
							mbInfoFlag = true;
						}
					});
				} else {
					new SetConfig(mContext).request(pms.getMsgFlag(), pms.getNotiFlag(), new APICallback() {
						@Override
						public void response (String arg0, JSONObject arg1) {
							mTxtResult.setText(arg1.toString());
							mbInfoFlag = false;
						}
					});
				}
			} else if (id == R.id.btn_new_msg) {
				mTxtResult.setText("loading...");

				// 현재 가지고 있는 메시지의 max user msg id를 가져올 수 있습니다.
				String req = new Prefs(mContext).getString(PMS.PREF_MAX_USER_MSG_ID);

				/**
				 * 서버에서 메시지를 가져 와서 SQLite에 저장하는 request입니다. callback에서 pms.selectMsgList(1, 9999)과 같이 cursor를 가져와 쓰시거나, newMsg완료시점에 broadcasting을 하기
				 * 때문에, PMS.RECEIVER_REQUERY로 receiver를 받으셔서 메시지를 가져오셔도 됩니다. 파라미터 관련해서는 api문서를 참조하시면 되겠니다.
				 */
				new NewMsg(mContext).request(PMS.TYPE_NEXT, req, "-1", "1", "30", new APICallback() {

					@Override
					public void response (String arg0, JSONObject arg1) {
						// 메시지를 cursor형태로 가져올수 있습니다.
						Cursor c = pms.selectMsgList(1, 9999);
						mTxtResult.setText(arg1.toString());
						// PMSDB db = PMSDB.getInstance(mContext);
						// db.showAllTable(Msg.TABLE_NAME);
						// c.moveToLast();
						//
						// if (c.getCount() > 0) {
						// Msg msg = new Msg(c);
						//
						// JSONObject json = new JSONObject();
						// try {
						// json.put(IPMSConsts.KEY_MSG_ID, msg.msgId);
						// json.put(IPMSConsts.KEY_NOTI_TITLE, msg.pushTitle);
						// json.put(IPMSConsts.KEY_NOTI_MSG, msg.pushMsg);
						// json.put(IPMSConsts.KEY_NOTI_IMG, msg.pushImg);
						// json.put(IPMSConsts.KEY_MSG, msg.msgText);
						// json.put(IPMSConsts.KEY_SOUND, "default");
						// json.put(IPMSConsts.KEY_MSG_TYPE, msg.msgType);
						//
						// JSONObject data = new JSONObject();
						// data.put("l", msg.appLink);
						//
						// json.put(IPMSConsts.KEY_DATA, data.toString());
						// } catch (JSONException e) {
						// e.printStackTrace();
						// }
						//
						// broadcastReceivedMessage(json.toString());
						// }
					}
				});
			}
		}
	};

}
