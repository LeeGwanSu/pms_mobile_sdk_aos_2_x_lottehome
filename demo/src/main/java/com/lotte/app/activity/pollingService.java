package com.lotte.app.activity;

import org.json.JSONObject;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.IBinder;

import com.pms.sdk.PMS;
import com.pms.sdk.api.APIManager.APICallback;
import com.pms.sdk.api.request.NewMsg;
import com.pms.sdk.common.util.CLog;
import com.pms.sdk.common.util.Prefs;

public class pollingService extends Service {

	private final static String TAG = "POLLING";

	private Context mCon = null;

	@Override
	public IBinder onBind (Intent intent) {
		return null;
	}

	@Override
	public void onCreate () {
		super.onCreate();

		mCon = getApplicationContext();

		registerReceiver(testNewMsg, new IntentFilter("com.pms.test.newMsg"));
	}

	@Override
	public int onStartCommand (Intent intent, int flags, int startId) {
		setAlarm();
		return START_FLAG_REDELIVERY;
	}

	@Override
	public void onStart (Intent intent, int startId) {
	}

	@Override
	public void onDestroy () {
		unregisterReceiver(testNewMsg);
		stopSelf();
		super.onDestroy();
	}

	private void setAlarm () {
		CLog.e(TAG, "Set Alarm!!!");
		PendingIntent pi = PendingIntent.getBroadcast(mCon, 0, new Intent("com.pms.test.newMsg"), PendingIntent.FLAG_UPDATE_CURRENT);

		AlarmManager alarmMgr = (AlarmManager) getSystemService(ALARM_SERVICE);
		alarmMgr.set(AlarmManager.RTC_WAKEUP, (System.currentTimeMillis() + 1000), pi);
	}

	private BroadcastReceiver testNewMsg = new BroadcastReceiver() {

		@Override
		public void onReceive (Context context, Intent intent) {
			CLog.e(TAG, "API CAll");
			String req = new Prefs(mCon).getString(PMS.PREF_MAX_USER_MSG_ID);
			new NewMsg(mCon).request(PMS.TYPE_NEXT, req, "-1", "1", "30", new APICallback() {
				@Override
				public void response (String arg0, JSONObject arg1) {
					setAlarm();
				}
			});
		}
	};
}
