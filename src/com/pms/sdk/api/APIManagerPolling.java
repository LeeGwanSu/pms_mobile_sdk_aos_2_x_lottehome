package com.pms.sdk.api;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.os.SystemClock;

import com.pms.sdk.IPMSConsts;
import com.pms.sdk.api.APIManager.APICallback;
import com.pms.sdk.api.request.DeviceCert;
import com.pms.sdk.bean.Logs;
import com.pms.sdk.common.security.SA2Dec;
import com.pms.sdk.common.security.SA2Enc;
import com.pms.sdk.common.util.CLog;
import com.pms.sdk.common.util.DateUtil;
import com.pms.sdk.common.util.PMSUtil;
import com.pms.sdk.common.util.PhoneState;
import com.pms.sdk.common.util.Prefs;
import com.pms.sdk.common.util.StringUtil;
import com.pms.sdk.db.PMSDB;

public class APIManagerPolling implements IPMSConsts {

	private Context mContext;
	private Prefs mPrefs;
	private JSONObject tempParams;
	private String authStatus;
	private Logs mLogs;
	private PMSDB mDB;
	private Boolean mbApiState = false;

	public APIManagerPolling(Context context) {
		this.mContext = context;
		this.mPrefs = new Prefs(mContext);
		this.mLogs = new Logs();
		this.mDB = PMSDB.getInstance(mContext);
	}

	public synchronized void call (final String url, final JSONObject params, final APICallback apiCallback) throws Exception {
		CLog.i("API:request=" + url);
		try {
			// save call log
			if (url.equals(API_COLLECT_LOG)) {
				mbApiState = false;
			} else {
				mbApiState = true;
			}

			if (mbApiState) {
				mLogs.date = DateUtil.getNowDateMo();
				mLogs.time = DateUtil.getNowTime();
				mLogs.logFlag = Logs.TYPE_A;
				mLogs.api = url.substring(0, url.length() - 2);
				mLogs.param = params.toString();
			}

			// check network available
			if (!PhoneState.getWifiState(mContext) && !PhoneState.get3GState(mContext)) {
				throw new APIException(CODE_CONNECTION_ERROR, "network not available");
			}

			// check url is null
			if (url == null || "".equals(url)) {
				throw new APIException(CODE_URL_IS_NULL, "url is null");
			}
			final String apiServerUrl = PMSUtil.getServerUrl(mContext);
			CLog.d(" SERVER_URL + url = " + apiServerUrl + url);

			// DeviceCert Check
			authStatus = PMSUtil.getDeviceCertStatus(mContext);
			CLog.d(" DeviceCert Status -> " + authStatus);
			if (!DEVICECERT_COMPLETE.equals(authStatus) && (!API_DEVICE_CERT.equals(url) && !API_COLLECT_LOG.equals(url))) {
				PMSUtil.setDeviceCertStatus(mContext, DEVICECERT_PROGRESS);
				new DeviceCert(mContext).request(null, new APICallback() {
					@Override
					public void response (String code, JSONObject json) {
						try {
							if (CODE_SUCCESS.equals(code)) {
								PMSUtil.setDeviceCertStatus(mContext, DEVICECERT_COMPLETE);
								call(url, params, apiCallback);
							} else {
								PMSUtil.setDeviceCertStatus(mContext, DEVICECERT_FAIL);
								if (apiCallback != null) {
									apiCallback.response(code, json);
								}
							}
						} catch (Exception e) {
							CLog.e(e.getMessage());
						}
					}
				});
				return;
			}

			// check param is null
			if (params == null || "".equals(params.toString())) {
				throw new APIException(CODE_PARAMS_IS_NULL, "params are null");
			}
			CLog.d(" params(json) = " + params.toString());

			// encrypt
			final String encryptedParam = encrypt(url, params.toString());
			CLog.d(" encryptedParam:" + encryptedParam);

			// call http
			tempParams = params;
			new Thread(new Runnable() {
				@Override
				public void run () {
					HttpURLConnection conn = null;

					try {
						URL urlTemp = new URL(apiServerUrl + url);

						conn = (HttpURLConnection) urlTemp.openConnection();
						conn.setConnectTimeout(10 * 1000);
						conn.setReadTimeout(3 * 1000);
						conn.setRequestMethod("POST");
						conn.setRequestProperty("Cache-Control", "no-cache");
						conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
						conn.setDoOutput(true);
						conn.setDoInput(true);

						StringBuilder encodedParams = new StringBuilder();
						try {
							encodedParams.append(URLEncoder.encode(KEY_API_DEFAULT, "UTF-8"));
							encodedParams.append('=');
							encodedParams.append(URLEncoder.encode(encryptedParam, "UTF-8"));
							encodedParams.append('&');
						} catch (UnsupportedEncodingException e1) {
						}

						CLog.d(" params(map):" + encodedParams.toString());

						OutputStream os = conn.getOutputStream();
						os.write(encodedParams.toString().getBytes());
						os.flush();

						int responseCode = conn.getResponseCode();
						if (responseCode == HttpURLConnection.HTTP_OK) {
							InputStream is = conn.getInputStream();
							ByteArrayOutputStream baos = new ByteArrayOutputStream();
							byte[] byteBuffer = new byte[1024];
							byte[] byteData = null;
							int nLength = 0;
							while ((nLength = is.read(byteBuffer, 0, byteBuffer.length)) != -1) {
								baos.write(byteBuffer, 0, nLength);
							}
							byteData = baos.toByteArray();

							proccessResult(url, new String(byteData), apiCallback);

							is.close();
							baos.close();
						} else {
							// error result
							try {
								JSONObject errResult = new JSONObject();
								errResult.put(KEY_API_CODE, CODE_CONNECTION_ERROR);
								errResult.put(KEY_API_MSG, "HTTP Response Error " + conn.getResponseCode());
								proccessResult(url, errResult.toString(), apiCallback);
							} catch (Exception e) {
								e.printStackTrace();
							}
						}
						os.close();
						conn.disconnect();
					} catch (MalformedURLException e) {
						e.printStackTrace();
						try {
							JSONObject errResult = new JSONObject();
							errResult.put(KEY_API_CODE, CODE_CONNECTION_ERROR);
							errResult.put(KEY_API_MSG, e.getMessage());
							proccessResult(url, errResult.toString(), apiCallback);
						} catch (Exception ev) {
							ev.printStackTrace();
						}
					} catch (IOException e) {
						e.printStackTrace();
						try {
							JSONObject errResult = new JSONObject();
							errResult.put(KEY_API_CODE, CODE_CONNECTION_ERROR);
							errResult.put(KEY_API_MSG, e.getMessage());
							proccessResult(url, errResult.toString(), apiCallback);
						} catch (Exception ev) {
							ev.printStackTrace();
						}
					}
				}
			}).start();
		} catch (Exception e) {
			e.printStackTrace();
			String code = e instanceof APIException ? ((APIException) e).getCode() : CODE_CONNECTION_ERROR;
			String msg = e instanceof APIException ? ((APIException) e).getMsg() : e.getMessage();

			JSONObject errResult = new JSONObject();
			errResult.put(KEY_API_CODE, code);
			errResult.put(KEY_API_MSG, msg);

			CLog.e(msg);

			if (apiCallback != null) {
				apiCallback.response(code, errResult);
			}
		}
	}

	/**
	 * processResult
	 * 
	 * @param context
	 * @param url
	 * @param object
	 * @param apiCallback
	 */
	private boolean proccessResult (final String url, final String object, final APICallback apiCallback) {
		String code = null;
		String msg = null;
		JSONObject result = null;

		try {
			// check result string
			if (object == null || "".equals(object)) {
				throw new APIException(CODE_NOT_RESPONSE, "not response");
			}
			CLog.d(" result:" + object);

			// decrypt
			String decryptedString = decrypt(url, object.toString());
			CLog.d(" result decrypted String:" + decryptedString);

			if (mbApiState) {
				mLogs.result = decryptedString;
				if (FLAG_Y.equals(mPrefs.getString(PREF_API_LOG_FLAG))) {
					mDB.insertLog(mLogs);
				}
			}

			// check live session
			long nowTime = SystemClock.currentThreadTimeMillis();

			if (url.indexOf(API_DEVICE_CERT) < 0) {
				// session time out
				long lastApiTime = Long.parseLong(mPrefs.getString(PREF_LAST_API_TIME));
				CLog.i(" diffTime:" + (nowTime - lastApiTime));
				if (lastApiTime + EXPIRE_RETAINED_TIME < nowTime) {
					throw new APIException(CODE_WRONG_SESSION, "expired session");
				}
			}
			mPrefs.putString(PREF_LAST_API_TIME, nowTime + "");

			// set result
			result = new JSONObject(decryptedString);
			code = result.getString(KEY_API_CODE);
			msg = result.getString(KEY_API_MSG);

			// check result code
			if (CODE_SUCCESS.equals(code)) {
				// success
				CLog.i("API:success");
			} else {
				// error
				throw new APIException(code, msg);
			}

			if (apiCallback != null) {
				apiCallback.response(code, result);
			}
		} catch (Exception e) {
			e.printStackTrace();
			if (e instanceof APIException && CODE_WRONG_SESSION.equals(((APIException) e).getCode())) {
				// wrong session

				PMSUtil.setDeviceCertStatus(mContext, DEVICECERT_PENDING);
				new DeviceCert(mContext).request(null, new APICallback() {
					@Override
					public void response (String code, JSONObject json) {
						try {
							if (CODE_SUCCESS.equals(code)) {
								PMSUtil.setDeviceCertStatus(mContext, DEVICECERT_COMPLETE);
								call(url, tempParams, apiCallback);
							}
						} catch (Exception e) {
							CLog.e(e.getMessage());
							// e.printStackTrace();
						}
					}
				});
			} else {
				if (apiCallback != null) {
					apiCallback.response(code, result);
				}
			}
			return false;
		}
		return true;
	}

	/**
	 * encrypt
	 * 
	 * @param url
	 * @param params
	 * @return
	 * @throws JSONException
	 */
	private String encrypt (String url, String params) throws Exception {
		if (isDefaultEncKey(url)) {
			return SA2Enc.encode(params, DEFAULT_ENC_KEY);
		} else {
			JSONObject encryptedJobj = new JSONObject();
			String appUserId = PMSUtil.getAppUserId(mContext);
			String encKey = PMSUtil.getEncKey(mContext);
			if (StringUtil.isEmptyArr(new String[] { appUserId, encKey })) {
				throw new APIException(CODE_SESSION_EXPIRED, "appUserId or encKey is null, have to call deviceCert before");
			} else {
				encryptedJobj.put(KEY_APP_USER_ID, appUserId);
				encryptedJobj.put(KEY_ENC_PARAM, SA2Enc.encode(params, encKey));
				return SA2Enc.encode(encryptedJobj.toString(), DEFAULT_ENC_KEY);
			}
		}
	}

	/**
	 * decrypt
	 * 
	 * @param url
	 * @param object
	 * @return
	 * @throws Exception
	 */
	private String decrypt (String url, String object) throws Exception {
		String result;
		try {
			result = SA2Dec.decrypt(object, getEncKey(url));
			if (result == null) {
				result = object;
			}
		} catch (Exception e) {
			// wrong session인 경우 암호화지 않은 상태에서 데이터가 넘어온다.
			result = object;
		}
		return result;
	}

	/**
	 * get enc key
	 * 
	 * @param url
	 * @return
	 */
	private String getEncKey (String url) {
		if (isDefaultEncKey(url)) {
			return DEFAULT_ENC_KEY;
		} else {
			return PMSUtil.getEncKey(mContext);
		}
	}

	/*
	 * is default encrypt key
	 */
	private boolean isDefaultEncKey (String url) {
		if (url.indexOf(API_DEVICE_CERT) > -1) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * https
	 * 
	 * @param url
	 * @return
	 */
	@SuppressWarnings("unused")
	private boolean isHttps (String url) {
		return false;
	}

	/**
	 * is multipart
	 * 
	 * @param url
	 * @return
	 */
	@SuppressWarnings("unused")
	private boolean isMultipart (String url) {
		return false;
	}
}
