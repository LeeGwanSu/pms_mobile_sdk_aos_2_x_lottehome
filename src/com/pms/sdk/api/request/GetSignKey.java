package com.pms.sdk.api.request;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;

import com.pms.sdk.api.APIManager.APICallback;
import com.pms.sdk.common.util.DataKeyUtil;
import com.pms.sdk.common.util.PMSUtil;

public class GetSignKey extends BaseRequest {

	public GetSignKey(Context context) {
		super(context);
	}

	/**
	 * get param
	 *
	 * @param appKey
	 * @param notiFlag
	 * @return
	 */
	public JSONObject getParam () {
		JSONObject jobj;

		try {
			jobj = new JSONObject();
			jobj.put("appKey", PMSUtil.getApplicationKey(mContext));
			jobj.put("pUrl", PMSUtil.getMQTTServerUrl(mContext));
			return jobj;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	/**
	 * request
	 *
	 * @param apiCallback
	 */
	public void request (final APICallback apiCallback) {
		try {
			apiManager.call(API_GET_SIGNKEY, getParam(), new APICallback() {
				@Override
				public void response (String code, JSONObject json) {
					if (CODE_SUCCESS.equals(code)) {
						requiredResultProc(json);
					}
					if (apiCallback != null) {
						apiCallback.response(code, json);
					}
				}
			});
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * required result proccess
	 *
	 * @param json
	 */
	private boolean requiredResultProc (JSONObject json) {
		try {
			DataKeyUtil.setDBKey(mContext, DB_SSL_SIGN_KEY, json.getString("signKey"));
			DataKeyUtil.setDBKey(mContext, DB_SSL_SIGN_PASS, json.getString("signPw"));
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return true;
	}
}
