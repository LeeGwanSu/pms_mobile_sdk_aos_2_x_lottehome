package com.pms.sdk.api.request;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.content.Intent;

import com.pms.sdk.IPMSConsts;
import com.pms.sdk.api.APIManager.APICallback;
import com.pms.sdk.api.APIManagerPolling;
import com.pms.sdk.bean.Msg;
import com.pms.sdk.common.util.CLog;
import com.pms.sdk.common.util.DateUtil;
import com.pms.sdk.common.util.Prefs;
import com.pms.sdk.db.PMSDB;
import com.pms.sdk.push.PushReceiver;

public class NewMsg implements IPMSConsts {

	private String pType;
	private String pReqUserMsgId;
	private String pMsgGrpCode;
	private String pPageNum;
	private String pPageSize;
	private APICallback pApiCallback;

	protected Context mContext;
	protected PMSDB mDB;
	protected Prefs mPrefs;

	protected boolean isComplete;

	private APIManagerPolling apiManagerPolling;

	public NewMsg(Context context) {
		mPrefs = new Prefs(context);
		mDB = PMSDB.getInstance(context);
		mContext = context;
		apiManagerPolling = new APIManagerPolling(context);
	}

	/**
	 * get param
	 * 
	 * @return
	 */
	public JSONObject getParam (String type, String reqUserMsgId, String msgGrpCode, String pageNum, String pageSize) {
		JSONObject jobj;

		try {
			jobj = new JSONObject();

			jobj.put("type", type);
			jobj.put("reqUserMsgId", reqUserMsgId);
			jobj.put("msgGrpCd", msgGrpCode);

			JSONObject page = new JSONObject();
			page.put("page", pageNum);
			page.put("row", pageSize);

			jobj.put("pageInfo", page);

			return jobj;
		} catch (JSONException e) {
			e.printStackTrace();
			return null;
		}
	}

	/**
	 * request
	 * 
	 * @param apiCallback
	 */
	public void request (String type, String reqUserMsgId, String msgGrpCode, String pageNum, String pageSize, final APICallback apiCallback) {

		isComplete = false;

		pType = type;
		pReqUserMsgId = reqUserMsgId;
		pMsgGrpCode = msgGrpCode;
		pPageNum = pageNum;
		pPageSize = pageSize;
		pApiCallback = apiCallback;

		try {
			apiManagerPolling.call(IPMSConsts.API_NEW_MSG, getParam(type, reqUserMsgId, msgGrpCode, pageNum, pageSize), new APICallback() {
				@Override
				public void response (String code, JSONObject json) {
					if (CODE_SUCCESS.equals(code)) {
						requiredResultProc(json);
					} else {
						isComplete = true;
					}

					if (apiCallback != null && isComplete) {
						apiCallback.response(code, json);
					}
				}
			});
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * required result proccess
	 * 
	 * @param json
	 */
	private boolean requiredResultProc (JSONObject json) {
		try {
			// recent msg map
			Map<String, Msg> newMsgList = new HashMap<String, Msg>();

			mPrefs.putString(IPMSConsts.PREF_MAX_USER_MSG_ID, json.getString("maxUserMsgId"));

			// get msg list (json array)
			JSONArray msgList = json.getJSONArray("msgs");

			int msgListSize = msgList.length();
			CLog.i("msgListSize=" + msgListSize);
			if (msgListSize < 1) {
				isComplete = true;
				return true;
			}
			for (int i = 0; i < msgListSize; i++) {
				Msg msg = new Msg(msgList.getJSONObject(i));
				if (Long.parseLong(msg.expireDate) / 1000000 < Long.parseLong(DateUtil.getNowDate()) / 1000000) {
					continue;
				}

				Msg exMsg = mDB.selectMsg(msg.userMsgId);
				if (exMsg != null) {
					// update msg (already exist)
					if (Msg.TYPE_H.equals(msg.msgType) || Msg.TYPE_L.equals(msg.msgType)) {
						// rich
						msg.msgGrpCd = "-1";
					}
					mDB.updateMsg(msg);
				} else {
					// insert msg (no exist)
					/**
					 * 2013.07.23 msgType이 H일 경우 msgCode를 음수로 저장
					 */
					if (Msg.TYPE_H.equals(msg.msgType) || Msg.TYPE_L.equals(msg.msgType)) {
						int minMsgCode = mDB.selectMinMsgCode();
						minMsgCode = minMsgCode > 0 ? 0 : minMsgCode;
						msg.msgGrpCd = String.valueOf(minMsgCode - 1);
						// rich 메시지인경우 바로 msggrp을 만들어 줌
						mDB.insertMsgGrp(msg);
					}
					mDB.insertMsg(msg);
				}

				if ((!Msg.TYPE_H.equals(msg.msgType) || !Msg.TYPE_L.equals(msg.msgType))
						&& (!newMsgList.containsKey(msg.msgGrpCd) || (newMsgList.containsKey(msg.msgGrpCd)
								&& Long.parseLong(newMsgList.get(msg.msgGrpCd).regDate) <= Long.parseLong(msg.regDate) && Long.parseLong(newMsgList
								.get(msg.msgGrpCd).msgId) < Long.parseLong(msg.msgId)))) {
					// rich메시지가 아니면서,
					// 같은 msgCode를 가진 msg가 map에 존재하지 않거나,
					// 같은 msgCode를 가진 msg가 map에 존재하면서, regDate가 같거나 크고, msgId가 큰 경우
					newMsgList.put(msg.msgGrpCd, msg);
				}
			}

			// set msg grp (recent)
			Iterator<String> it = newMsgList.keySet().iterator();
			while (it.hasNext()) {
				String recentMsgCode = it.next();

				// set recent msg
				Msg recentMsg = newMsgList.get(recentMsgCode);
				if (mDB.selectMsgGrp(recentMsgCode) != null) {
					// update msg grp (already exist)
					mDB.updateRecentMsgGrp(recentMsg);
				} else {
					// insert msg grp (no exist)
					mDB.insertMsgGrp(recentMsg);
				}
			}

			try {
				if (msgListSize >= Integer.parseInt(pPageSize)) {
					request(pType, pReqUserMsgId, pMsgGrpCode, (Integer.parseInt(pPageNum) + 1) + "", pPageSize, pApiCallback);
				} else {
					isComplete = true;
				}
			} catch (Exception e) {
				e.printStackTrace();
			}

			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		} finally {
			mDB.deleteExpireMsg();
			mDB.deleteEmptyMsgGrp();

			// requery
//			mContext.sendBroadcast(new Intent(mContext, PushReceiver.class).setAction(IPMSConsts.RECEIVER_REQUERY));
		}
	}
}
