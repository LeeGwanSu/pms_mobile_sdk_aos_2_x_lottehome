package com.pms.sdk.api.request;

import android.content.Context;

import com.pms.sdk.api.APIManager.APICallback;
import com.pms.sdk.db.PMSDB;

import org.json.JSONArray;
import org.json.JSONObject;

public class ReadMsg extends BaseRequest {

	public ReadMsg(Context context) {
		super(context);
	}

	/**
	 * get param
	 * 
	 * @return
	 */
	public JSONObject getParam (String msgGrpCd, String firstUserMsgId, String lastUserMsgId) {
		JSONObject jobj;

		try {
			jobj = new JSONObject();
			jobj.put("msgGrpCd", msgGrpCd);
			jobj.put("firstUserMsgId", firstUserMsgId);
			jobj.put("lastUserMsgId", lastUserMsgId);

			return jobj;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	/**
	 * get param
	 * 
	 * @param userMsgIds
	 * @param msgIds
	 * @return
	 */
	public JSONObject getParam (JSONArray userMsgIds, JSONArray msgIds) {
		JSONObject jobj;

		try {
			jobj = new JSONObject();
			jobj.put("userMsgIds", userMsgIds);
			jobj.put("msgIds", msgIds);

			return jobj;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	/**
	 * get Param
	 *
	 * @param reads
	 * @return
	 */
	public JSONObject getParam (JSONArray reads) {
		JSONObject jobj;

		try {
			jobj = new JSONObject();
			jobj.put("reads", reads);

			return jobj;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	/**
	 * request
	 * 
	 * @param apiCallback
	 */
	public void request (final String msgGrpCd, final String firstUserMsgId, final String lastUserMsgId, final APICallback apiCallback) {
		try {
			apiManager.call(API_READ_MSG, getParam(msgGrpCd, firstUserMsgId, lastUserMsgId), new APICallback() {
				@Override
				public void response (String code, JSONObject json) {
					PMSDB db = PMSDB.getInstance(mContext);
					db.updateReadMsg(msgGrpCd, firstUserMsgId, lastUserMsgId);
					db.updateNewMsgCnt();

					requiredResultProc(json);
					if (apiCallback != null) {
						apiCallback.response(code, json);
					}
				}
			});
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * request
	 * 
	 * @param userMsgIds
	 * @param msgIds
	 * @param apiCallback
	 */
	public void request (final JSONArray userMsgIds, final JSONArray msgIds, final APICallback apiCallback) {
		try {
			apiManager.call(API_READ_MSG, getParam(userMsgIds, msgIds), new APICallback() {
				@Override
				public void response (String code, JSONObject json) {
					if (CODE_SUCCESS.equals(code)) {
						PMSDB db = PMSDB.getInstance(mContext);

						int userMsgIdsSize = userMsgIds != null ? userMsgIds.length() : 0;
						int msgIdsSize = msgIds != null ? msgIds.length() : 0;

						for (int i = 0; i < userMsgIdsSize; i++) {
							try {
								db.updateReadMsgWhereUserMsgId(userMsgIds.getString(i));
							} catch (Exception e) {
								e.printStackTrace();
							}
						}
						for (int i = 0; i < msgIdsSize; i++) {
							try {
								db.updateReadMsgWhereMsgId(msgIds.getString(i));
							} catch (Exception e) {
								e.printStackTrace();
							}
						}

						db.updateNewMsgCnt();

						requiredResultProc(json);
					}
					if (apiCallback != null) {
						apiCallback.response(code, json);
					}
				}
			});
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * request
	 *
	 * @param reads
	 * @param apiCallback
	 */
	public void request (final JSONArray reads, final APICallback apiCallback) {
		try {
			apiManager.call(API_READ_MSG, getParam(reads), new APICallback() {
				@Override
				public void response (String code, JSONObject json) {
					if (CODE_SUCCESS.equals(code)) {
						PMSDB db = PMSDB.getInstance(mContext);

						for (int i = 0; i < reads.length(); i++) {
							try {
								db.updateReadMsgWhereMsgId(reads.getJSONObject(i).getString("msgId"));
							} catch (Exception e) {
								e.printStackTrace();
							}
						}

						db.updateNewMsgCnt();

						requiredResultProc(json);
					}
					if (apiCallback != null) {
						apiCallback.response(code, json);
					}
				}
			});
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * required result proccess
	 * 
	 * @param json
	 */
	private boolean requiredResultProc (JSONObject json) {
		// requery
//		mContext.sendBroadcast(new Intent(mContext, Badge.class).setAction(IPMSConsts.RECEIVER_REQUERY));
		return true;
	}
}
