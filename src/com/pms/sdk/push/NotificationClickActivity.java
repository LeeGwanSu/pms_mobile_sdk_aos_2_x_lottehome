package com.pms.sdk.push;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;

import androidx.annotation.Nullable;

import com.pms.sdk.IPMSConsts;
import com.pms.sdk.api.APIManager;
import com.pms.sdk.api.request.ReadMsg;
import com.pms.sdk.bean.PushMsg;
import com.pms.sdk.common.util.CLog;
import com.pms.sdk.common.util.PMSUtil;
import com.pms.sdk.common.util.StringUtil;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class NotificationClickActivity extends Activity implements IPMSConsts {
    private Context context;
    private Intent intent;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.context = getApplicationContext();
        this.intent = getIntent();
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        this.context = getApplicationContext();
        this.intent = intent;
    }

    public boolean isAvailablePush(Bundle extras){
        boolean isAvailable = true;
        PushMsg pushMsg = new PushMsg(extras);
        if (StringUtil.isEmpty(pushMsg.msgId)
                || StringUtil.isEmpty(pushMsg.notiTitle)
                || StringUtil.isEmpty(pushMsg.notiMsg)
                || StringUtil.isEmpty(pushMsg.msgType)) {
            isAvailable = false;
        }
        return isAvailable;
    }

    public void requestReadMsg(final APIManager.APICallback callback) {
        if(context == null || intent == null){
            CLog.e("[NotificationClickActivity] must be declared \"super.onNewIntent(intent);\" first");
            return;
        }
        if(callback == null){
            CLog.i("[NotificationClickActivity] requestReadMsg() callback is null");
        }

        PushMsg pushMsg = new PushMsg(intent.getExtras());
        String msgId = pushMsg.msgId;
        if(TextUtils.isEmpty(msgId)){
            CLog.i("[NotificationClickActivity] requestReadMsg() msgId is null");
            return;
        }

        ReadMsg api = new ReadMsg(context);
        JSONArray jsonArray = new JSONArray();
        jsonArray.put(PMSUtil.getReadParam(msgId));
        api.request(jsonArray, callback);
    }

    public PushMsg getPushMsg() {
        if(context == null || intent == null){
            CLog.e("[NotificationClickActivity] must be declared \"super.onNewIntent(intent);\" first");
            return null;
        }
        PushMsg pushMsg = new PushMsg(intent.getExtras());
        return pushMsg;
    }
    public String getAppLink()
    {
        if(context == null || intent == null){
            CLog.e("[NotificationClickActivity] must be declared \"super.onNewIntent(intent);\" first");
            return null;
        }
        PushMsg pushMsg = new PushMsg(intent.getExtras());
        String data = pushMsg.data;
        if(TextUtils.isEmpty(data)){
            CLog.e("[NotificationClickActivity] getPushMsg() onParsed error");
            return null;
        }
        try {
            JSONObject object = new JSONObject(data);
            String appLinkUrl = object.getString("l");
            if(TextUtils.isEmpty(appLinkUrl)){
                CLog.i("[NotificationClickActivity] getPushMsg() appLinkUrl is empty");
                return null;
            }
            return appLinkUrl;
        }
        catch (JSONException e) {
            CLog.e("[NotificationClickActivity] getPushMsg() onParsed error");
            return null;
        }
    }
}
