package com.pms.sdk.push;

import java.io.ByteArrayInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;

import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManagerFactory;

/**
 * 
 * @author erzisk
 * @since 2013.06.04
 */
public class SelfSignedSocketFactory {

	public static SSLSocketFactory getSSLSocketFactory (ByteArrayInputStream keyStore, String password) {
		try {
			CertificateFactory certificateFactory = CertificateFactory.getInstance("X.509");
			X509Certificate cert = (X509Certificate) certificateFactory.generateCertificate(keyStore);
			String alias = cert.getSubjectX500Principal().getName();

			KeyStore trustStore = KeyStore.getInstance(KeyStore.getDefaultType());
			trustStore.load(null, null);
			trustStore.setCertificateEntry(alias, cert);

			TrustManagerFactory tmf = TrustManagerFactory.getInstance("X509");
			tmf.init(trustStore);

			SSLContext ctx = SSLContext.getInstance("TLS");
			ctx.init(null, tmf.getTrustManagers(), null);

			SSLSocketFactory sslSockFactory = ctx.getSocketFactory();
			return sslSockFactory;

		} catch (KeyStoreException e) {
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		} catch (KeyManagementException e) {
			e.printStackTrace();
		} catch (CertificateException e) {
			e.printStackTrace();
		}
		return null;
	}

	public static SSLSocketFactory ApiSSLSocketFactory (String protocol) {
		SSLContext sc = null;
		try {
			sc = SSLContext.getInstance(protocol);
			sc.init(null, null, null);

		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		} catch (KeyManagementException e) {
			e.printStackTrace();
		}

		return sc.getSocketFactory();
	}
}
