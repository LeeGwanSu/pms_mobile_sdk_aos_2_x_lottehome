package com.pms.sdk.push;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebSettings.PluginState;
import android.webkit.WebSettings.RenderPriority;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.FrameLayout.LayoutParams;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.pms.sdk.IPMSConsts;
import com.pms.sdk.bean.Msg;
import com.pms.sdk.bean.PushMsg;
import com.pms.sdk.common.util.CLog;
import com.pms.sdk.common.util.PMSUtil;
import com.pms.sdk.common.util.PhoneState;
import com.pms.sdk.common.util.Prefs;
import com.pms.sdk.common.util.StringUtil;

/**
 * push popup activity
 * 
 * @author erzisk
 * @since 2013.06.07
 */
public class PushPopupActivity extends Activity implements IPMSConsts {

	private Context mContext;
	private Prefs mPrefs;

	private FrameLayout mLayBack;
	private FrameLayout mLayOuter;

	private WebView mWv;

	private PushMsg pushMsg;

	private int pushPopupColor = 0;

	private int pushPopupShowingTime = 10000;
	private int mShowingTime;

	private final Thread mThread = new Thread(new Runnable() {
		@Override
		public void run () {
			while (mShowingTime > -1) {
				mShowingTime -= 1000;
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
			finish();
		}
	});

	@Override
	protected void onCreate (Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		mContext = this;

		mPrefs = new Prefs(this);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().addFlags(WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED);

		pushPopupShowingTime = mPrefs.getInt(PREF_PUSH_POPUP_SHOWING_TIME);
		pushPopupColor = mPrefs.getInt(PREF_PUSH_POPUP_COLOR);

		int[] deviceSize = PhoneState.getDeviceSize(this);
		int maxWidth = (int) Math.round((deviceSize[0] < deviceSize[1] ? deviceSize[0] : deviceSize[1]) * 0.8);
		// set view
		// back이 가장 바깥, outer가 contents
		mLayBack = new FrameLayout(this);
		mLayOuter = new FrameLayout(this);

		mLayBack.setLayoutParams(new LayoutParams(-2, -1));
		mLayBack.setPadding(50, 50, 50, 50);
		mLayBack.setBackgroundColor(mPrefs.getInt(PREF_PUSH_POPUP_BACKGROUND_COLOR));
		LayoutParams lp = new LayoutParams(maxWidth, -2);
		lp.gravity = Gravity.CENTER;

		mLayOuter.setLayoutParams(lp);

		mLayBack.addView(mLayOuter);
		setContentView(mLayBack);

		CLog.i("pushPopupColor:" + pushPopupColor);
		CLog.i("pushPopupBackgroundColor:" + mPrefs.getInt(PREF_PUSH_POPUP_BACKGROUND_COLOR));
		setPushPopup(getIntent());

		// // screen on
		// pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
		// wl = pm.newWakeLock(PowerManager.SCREEN_BRIGHT_WAKE_LOCK | PowerManager.ACQUIRE_CAUSES_WAKEUP, "Tag");
		// if (!pm.isScreenOn()) {
		// mLayBack.setBackgroundColor(0xFF000000);
		// wl.acquire();
		// }
		//
		// mFinishHandler.postDelayed(finishRunnable, DEFAULT_SHOWING_TIME);
	}

	@Override
	protected void onResume () {
		try {
			WebView.class.getMethod("onResume").invoke(mWv);
			mWv.resumeTimers();
		} catch (Exception e) {
		}
		super.onResume();
		this.overridePendingTransition(0, 0);
	}

	@Override
	protected void onNewIntent (Intent intent) {
		// TODO Auto-generated method stub
		super.onNewIntent(intent);
		setPushPopup(intent);
	}

	/**
	 * set rich popup
	 * 
	 * @param intent
	 */
	private void setPushPopup (Intent intent) {
		pushMsg = new PushMsg(intent.getExtras());

		CLog.i(pushMsg + "");
		mLayOuter.removeAllViews();

		if (Msg.TYPE_H.equals(pushMsg.msgType) || Msg.TYPE_L.equals(pushMsg.msgType)) {
			mLayOuter.addView(getRichPopup(mContext, pushMsg.message));
		} else {
			mLayOuter.addView(getTextPopup(mContext, pushMsg.notiMsg));
		}

		mShowingTime = pushPopupShowingTime;

		if (!mThread.isAlive()) {
			mThread.start();
		}
	}

	/**
	 * get rich popup
	 * 
	 * @param context
	 * @param url
	 * @return
	 */
	@SuppressLint("SetJavaScriptEnabled")
	@SuppressWarnings("deprecation")
	private View getRichPopup (Context context, String url) {
		CLog.i("getRichPopup");

		// [set layout]
		// set layoutParams
		LinearLayout.LayoutParams lllp1 = new LinearLayout.LayoutParams(0, -2);
		lllp1.weight = 1;
		LinearLayout.LayoutParams lllp2 = new LinearLayout.LayoutParams(-1, -2);
		lllp2.weight = 1;

		// layback
		LinearLayout layBack = new LinearLayout(context);
		layBack.setOrientation(LinearLayout.VERTICAL);
		layBack.setLayoutParams(new LayoutParams(-1, -1));
		layBack.setBackgroundColor(pushPopupColor);

		// laybutton
		LinearLayout layButton = new LinearLayout(context);
		layButton.setOrientation(LinearLayout.HORIZONTAL);
		layButton.setLayoutParams(new LayoutParams(-1, -2));
		Button btnOk = new Button(context);
		Button btnCancel = new Button(context);

		// webview
		mWv = new WebView(context);
		mWv.setLayoutParams(lllp2);

		// progress
		final ProgressBar prg = new ProgressBar(context, null, android.R.attr.progressBarStyleHorizontal);

		// add view
		layButton.addView(btnCancel, lllp1);
		layButton.addView(btnOk, lllp1);
		layBack.addView(mWv);
		layBack.addView(prg);
		layBack.addView(layButton);

		// [set attribute]
		// progress
		prg.setProgress(50);
		prg.setLayoutParams(lllp2);

		// set button text
		String confirmText = mPrefs.getString(PREF_POPUP_CONFIRM_TEXT);
		String cancelText = mPrefs.getString(PREF_POPUP_CANCEL_TEXT);
		if (StringUtil.isEmpty(confirmText)) {
			confirmText = "OK";
		}
		if (StringUtil.isEmpty(cancelText)) {
			cancelText = "CLOSE";
		}
		btnOk.setText(confirmText);
		btnCancel.setText(cancelText);

		// webview
		mWv.clearCache(true);
		mWv.clearHistory();
		mWv.clearFormData();
		mWv.clearView();

		mWv.setInitialScale(1);
		mWv.setBackgroundColor(0);
		mWv.setHorizontalScrollBarEnabled(false);
		mWv.setVerticalScrollBarEnabled(false);

		WebSettings settings = mWv.getSettings();
		settings.setJavaScriptEnabled(true);
		settings.setJavaScriptCanOpenWindowsAutomatically(true);
		settings.setSupportMultipleWindows(true);
		settings.setUseWideViewPort(true);
		settings.setLoadWithOverviewMode(true);
		settings.setPluginState(PluginState.ON);
		// settings.setLayoutAlgorithm(LayoutAlgorithm.SINGLE_COLUMN);
		settings.setRenderPriority(RenderPriority.HIGH);
		settings.setCacheMode(WebSettings.LOAD_NO_CACHE);
		settings.setGeolocationEnabled(true);
		settings.setDomStorageEnabled(true);
		settings.setUseWideViewPort(true);

		mWv.setWebChromeClient(new WebChromeClient() {
			@Override
			public void onProgressChanged (WebView view, int newProgress) {
				super.onProgressChanged(view, newProgress);
				prg.setProgress(newProgress);
				if (newProgress >= 100) {
					prg.setVisibility(View.GONE);
				}
			}
		});
		PMSWebViewBridge pmsBridge = new PMSWebViewBridge(mContext);
		mWv.addJavascriptInterface(pmsBridge, "pms");

		// setting html to webview
		if (Msg.TYPE_L.equals(pushMsg.msgType) && url.startsWith("http")) {
			mWv.loadUrl(url);
		} else {
			mWv.loadData(url, "text/html; charset=utf-8", null);
		}

		mWv.setOnTouchListener(new OnTouchListener() {
			@SuppressLint("ClickableViewAccessibility")
			@Override
			public boolean onTouch (View v, MotionEvent event) {
				if (mShowingTime < pushPopupShowingTime) {
					mShowingTime = pushPopupShowingTime;
				}
				if (event.getAction() != MotionEvent.ACTION_MOVE) {
					((WebView) v).setWebViewClient(new WebViewClient() {
						@Override
						public boolean shouldOverrideUrlLoading (WebView view, String url) {
							PMSUtil.arrayToPrefs(mContext, PREF_READ_LIST, pushMsg.msgId);
							CLog.i("webViewClient:url=" + url);
							Intent intent = new Intent(Intent.ACTION_VIEW);
							Uri uri = Uri.parse(url);
							intent.setData(uri);
							startActivity(intent);
							return true;
						}
					});
				}
				return false;
			}
		});

		mWv.setWebViewClient(new WebViewClient() {
			@Override
			public boolean shouldOverrideUrlLoading (WebView view, String url) {
				CLog.i("webViewClient:url=" + url);
				return super.shouldOverrideUrlLoading(view, url);
			}
		});

		btnOk.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick (View v) {
				PMSUtil.arrayToPrefs(mContext, PREF_READ_LIST, pushMsg.msgId);
				startNotiReceiver();
			}
		});
		btnCancel.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick (View v) {
				finish();
			}
		});

		return layBack;
	}

	/**
	 * get Text popup
	 * 
	 * @param context
	 * @param msg
	 * @return
	 */
	private View getTextPopup (Context context, String msg) {
		CLog.i("getTextPopup");

		// [set layout]
		// set layoutParams
		LinearLayout.LayoutParams lllp1 = new LinearLayout.LayoutParams(0, -2);
		lllp1.weight = 1;
		LinearLayout.LayoutParams lllp2 = new LinearLayout.LayoutParams(-1, -2);
		lllp2.setMargins(15, 15, 15, 15);

		// layback
		LinearLayout layBack = new LinearLayout(context);
		layBack.setOrientation(LinearLayout.VERTICAL);
		layBack.setLayoutParams(new LayoutParams(-1, -1));
		layBack.setBackgroundColor(pushPopupColor);

		// laybutton
		LinearLayout layButton = new LinearLayout(context);
		layButton.setOrientation(LinearLayout.HORIZONTAL);
		layButton.setLayoutParams(new LayoutParams(-1, -2));
		Button btnOk = new Button(context);
		Button btnCancel = new Button(context);

		// textview
		TextView txt = new TextView(context);
		txt.setLayoutParams(lllp2);

		// add view
		layButton.addView(btnCancel, lllp1);
		layButton.addView(btnOk, lllp1);

		layBack.addView(txt);
		layBack.addView(layButton);

		// [set attribute]
		// button text
		btnOk.setText(mPrefs.getString(PREF_POPUP_CONFIRM_TEXT));
		btnCancel.setText(mPrefs.getString(PREF_POPUP_CANCEL_TEXT));

		// msg (content)
		txt.setTextColor(0xFFFFFFFF);
		txt.setText(msg);

		// [set listener]
		btnOk.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick (View v) {
				PMSUtil.arrayToPrefs(mContext, PREF_READ_LIST, pushMsg.msgId);
				startNotiReceiver();
			}
		});
		btnCancel.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick (View v) {
				finish();
			}
		});

		return layBack;
	}

	/**
	 * noti receiver
	 */
	private void startNotiReceiver () {
		Prefs prefs = new Prefs(mContext);
		String receiverClass = prefs.getString(PREF_NOTI_RECEIVER);
		receiverClass = receiverClass != null ? receiverClass : "com.pms.sdk.notification";

		Intent intent = new Intent(receiverClass);
		intent.putExtras(getIntent().getExtras());
		mContext.sendBroadcast(intent);

		hideNotification();
	}

	/**
	 * hide notification
	 */
	private void hideNotification () {
		NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
		notificationManager.cancel(PushReceiver.NOTIFICATION_ID);
	}

	@Override
	public void finish () {
		super.finish();
		overridePendingTransition(0, 0);
	}
}