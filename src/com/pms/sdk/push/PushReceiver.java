package com.pms.sdk.push;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.ActivityManager;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.media.AudioAttributes;
import android.media.AudioManager;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.PowerManager;
import android.text.TextUtils;

import androidx.core.app.NotificationCompat;

import com.android.volley.Network;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.BasicNetwork;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.ImageLoader.ImageContainer;
import com.android.volley.toolbox.ImageLoader.ImageListener;
import com.android.volley.toolbox.NoCache;
import com.pms.sdk.IPMSConsts;
import com.pms.sdk.PMS;
import com.pms.sdk.bean.Msg;
import com.pms.sdk.bean.PushMsg;
import com.pms.sdk.common.util.*;
import com.pms.sdk.db.PMSDB;
import com.pms.sdk.view.BitmapLruCache;
import org.json.JSONObject;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

/**
 * push receiver
 *
 * @author erzisk
 * @since 2013.06.07
 */
public class PushReceiver extends BroadcastReceiver implements IPMSConsts {

	private static final String USE = "Y";

	public final static int NOTIFICATION_ID = 0x253470;
	private static final int NOTIFICATION_GROUP_SUMMARY_ID = 1;
	private static final String NOTIFICATION_GROUP = "com.pms.sdk.notification_type";
	private static int sNotificationId = NOTIFICATION_GROUP_SUMMARY_ID + 1;
	private Prefs mPrefs;

	private PowerManager pm;
	private PowerManager.WakeLock wl;

	private static final int DEFAULT_SHOWING_TIME = 30000;

	private final Handler mFinishHandler = new Handler();

	private Bitmap mPushImage;

	private Boolean mbPollingState = false;

	@Override
	public synchronized void onReceive (final Context context, final Intent intent)
	{
		if (intent == null || intent.getAction() == null)
		{
			if (intent != null)
			{
				CLog.e("intent action is null, intent : " + intent.toString());
			}

			return;
		}

		CLog.i("onReceive() -> " + intent.toString());
		mPrefs = new Prefs(context);

		if(IPMSConsts.ACTION_RECEIVE.equals(intent.getAction()))
		{
			String message = intent.getStringExtra(KEY_PUSH_MSG);
			if (!TextUtils.isEmpty(message))
			{
				try
				{
					JSONObject msgObj = new JSONObject(message);
					CLog.d("msgObj : " + msgObj.toString());

					if (msgObj.has(KEY_MSG_ID)) {
						intent.putExtra(KEY_MSG_ID, msgObj.getString(KEY_MSG_ID));
					}
					if (msgObj.has(KEY_NOTI_TITLE)) {
						intent.putExtra(KEY_NOTI_TITLE, msgObj.getString(KEY_NOTI_TITLE));
					}
					if (msgObj.has(KEY_MSG_TYPE)) {
						intent.putExtra(KEY_MSG_TYPE, msgObj.getString(KEY_MSG_TYPE));
					}
					if (msgObj.has(KEY_NOTI_MSG)) {
						intent.putExtra(KEY_NOTI_MSG, msgObj.getString(KEY_NOTI_MSG));
					}
					if (msgObj.has(KEY_MSG)) {
						intent.putExtra(KEY_MSG, msgObj.getString(KEY_MSG));
					}
					if (msgObj.has(KEY_SOUND)) {
						intent.putExtra(KEY_SOUND, msgObj.getString(KEY_SOUND));
					}
					if (msgObj.has(KEY_NOTI_IMG)) {
						intent.putExtra(KEY_NOTI_IMG, msgObj.getString(KEY_NOTI_IMG));
					}
					if (msgObj.has(KEY_DATA)) {
						intent.putExtra(KEY_DATA, msgObj.getString(KEY_DATA));
					}
				}
				catch (Exception e)
				{
					CLog.e(e.getMessage());
				}
			}

			if (isImagePush(intent.getExtras()))
			{
				CLog.d("isImagePush");
				try
				{
					// image push
					Network network = new BasicNetwork(new HurlStack(null, SelfSignedSocketFactory.ApiSSLSocketFactory("TLS")));
					RequestQueue queue = new RequestQueue(new NoCache(), network);
					queue.getCache().clear();
					queue.start();
					ImageLoader imageLoader = new ImageLoader(queue, new BitmapLruCache());
					imageLoader.get(intent.getStringExtra(KEY_NOTI_IMG), new ImageListener()
					{
						@Override
						public void onResponse (ImageContainer response, boolean isImmediate)
						{
							if (response == null) {
								CLog.e("response is null");
								return;
							}

							if (response.getBitmap() == null) {
								CLog.e("bitmap is null");
								return;
							}

							mPushImage = response.getBitmap();
							CLog.i("imageWidth:" + mPushImage.getWidth());
							onMessage(context, intent);
						}

						@Override
						public void onErrorResponse (VolleyError error)
						{
							CLog.e("onErrorResponse:" + error.getMessage());
						}
					});
				}
				catch (NullPointerException e)
				{
					CLog.e(e.getMessage());
				}
			}
			else
			{
				// default push
				CLog.d("isDefaultPush");
				onMessage(context, intent);
			}
		}
	}

	/**
	 * on message (gcm, private msg receiver)
	 *
	 * @param context
	 */
	@SuppressWarnings("deprecation")
	private synchronized void onMessage (final Context context, Intent intent) {
		CLog.d("synchronized void onMessage");

//		기존 UUID 남아 있는것들은 들어와 쌓이지 않게 아예 전화권한이 없는경우에는 다 버림

		//2019.11.20 Android 10 재설정할 수 없는 기기 식별자 제한 이슈로 해당 코드 동작 안하게 처리
//		if(!PhoneState.permissionChecker(context, Manifest.permission.READ_PHONE_STATE)) {
//			CLog.e("code:908 msg:permission denied phone state");
//			return;
//		}

		PMS pms = PMS.getInstance(context);
		final Bundle extras = intent.getExtras();

		PushMsg pushMsg = new PushMsg(extras);
		CLog.i("pushMsg parsed =  " + pushMsg);

		if (FLAG_Y.equals(mPrefs.getString(IPMSConsts.PREF_SCREEN_WAKEUP_FLAG)) || !StringUtil.isEmpty(mPrefs.getString(PREF_SCREEN_WAKEUP_FLAG)))
		{
			// screen on
			pm = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
			wl = pm.newWakeLock(PowerManager.SCREEN_DIM_WAKE_LOCK | PowerManager.ACQUIRE_CAUSES_WAKEUP, "myapp:mywakelocktag");
			if (!pm.isScreenOn())
			{
				wl.acquire();
				mFinishHandler.postDelayed(finishRunnable, DEFAULT_SHOWING_TIME);
			}
		}

//		if (TextUtils.isEmpty(pushMsg.msgId) || "".equals(pushMsg.msgId))
//		{
//			CLog.i("msgId or notiTitle or notiMsg or msgType is null");
//
//			String privateFlag = PMSUtil.getPrivateFlag(context);
//			String mktFlag = PMSUtil.getMQTTFlag(context);
//			CLog.d("privateFlag: " + privateFlag + ", mktFlag: " + mktFlag);
//
//			if(FLAG_Y.equals(privateFlag) && FLAG_Y.equals(mktFlag))
//			{
//				Intent i = new Intent(context, RestartReceiver.class);
//				i.setAction(MQTTService.ACTION_FORCE_START);
//				context.sendBroadcast(i);
//			}
//
//			return;
//		}

		PMSDB db = PMSDB.getInstance(context);

		CLog.i("MsgID Check Process => " + mbPollingState);
		if (mbPollingState == false) {
			// check already exist msg
			Msg existMsg = db.selectMsgWhereMsgId(pushMsg.msgId);
			if (existMsg != null && existMsg.msgId.equals(pushMsg.msgId)) {
				CLog.i("already exist msg");
				return;
			}

			// insert (temp) new msg
			Msg newMsg = new Msg();
			newMsg.readYn = Msg.READ_N;
			newMsg.msgGrpCd = "999999";
			newMsg.expireDate = "0";
			newMsg.msgId = pushMsg.msgId;

			db.insertMsg(newMsg);
		}

		// refresh list and badge
		Intent intentPush = null;

		String receiverClass = null;
//        receiverClass = ProPertiesFileUtil.getString(context, PRO_RECEIVER_CLASS);
		receiverClass = mPrefs.getString(IPMSConsts.PREF_PUSH_RECEIVER_CLASS);
		if (receiverClass != null) {
			try {
				Class<?> cls = Class.forName(receiverClass);
				intentPush = new Intent(context, cls).putExtras(extras);
				intentPush.setAction(RECEIVER_PUSH);
			} catch (ClassNotFoundException e) {
				CLog.e(e.getMessage());
			}
		}
		if (intentPush == null) {
			intentPush = new Intent(RECEIVER_PUSH).putExtras(extras);
		}

		if (intentPush != null) {
			context.sendBroadcast(intentPush);
		}

		// show noti
		CLog.i("version code :" + Build.VERSION.SDK_INT);

		if(DataKeyUtil.getDBKey(context, DB_DO_NOT_DISTURB_FLAG).equals(FLAG_Y))
		{
			boolean isDisturb = false;
			String doNotDisturbTime = DataKeyUtil.getDBKey(context, DB_DO_NOT_DISTURB_TIME);
			try
			{
				int fromHour = Integer.parseInt(doNotDisturbTime.substring(0, 2));
				int fromMinute = Integer.parseInt(doNotDisturbTime.substring(2, 4));
				int toHour = Integer.parseInt(doNotDisturbTime.substring(4, 6));
				int toMinute = Integer.parseInt(doNotDisturbTime.substring(6, 8));
				Date currentDate = new Date();
				Calendar calendar = GregorianCalendar.getInstance();
				calendar.setTime(currentDate);
				int currentHour = calendar.get(Calendar.HOUR_OF_DAY);
				int currentMinute = calendar.get(Calendar.MINUTE);

				int fromTimestamp = fromHour * 60 + fromMinute;
				if (fromHour > toHour)
				{
					toHour += 24;
				}
				int toTimestamp = toHour * 60 + toMinute;
				if (currentHour <= 12 && toHour >= 24)
				{
					currentHour += 24;
				}
				int currentTimestamp = currentHour * 60 + currentMinute;
				if (currentTimestamp >= fromTimestamp && currentTimestamp <= toTimestamp)
				{
					isDisturb = true;
				}
				CLog.i("doNotDisturbTime = " + doNotDisturbTime + " parsed->" + fromHour + ":" + fromMinute + "~" + toHour + ":" + toMinute + " currentTime" + currentHour + ":" + currentMinute);
			}
			catch (Exception e)
			{
				CLog.e(e.getMessage());
				isDisturb = false;
			}
			CLog.i(isDisturb ? "DoNotDisturbTime Y" : "DoNotDisturbTime N");

			if (!isDisturb)
			{
				// execute push noti listener
				if (pms.getOnReceivePushListener() != null) {
					if (pms.getOnReceivePushListener().onReceive(context, extras)) {
						showNotification(context, extras);
					}
				} else {
					showNotification(context, extras);
				}
			}
		}
		else
		{
			// ** 수정 ** 기존 코드는 PREF_MSG_FLAG로 되어 있어 NOTI_FLAG로 변경 함
			// CLog.i("NOTI FLAG : " + mPrefs.getString(PREF_NOTI_FLAG));
			if (USE.equals(mPrefs.getString(IPMSConsts.PREF_SCREEN_WAKEUP_FLAG)) || StringUtil.isEmpty(mPrefs.getString(PREF_SCREEN_WAKEUP_FLAG))) {
				// screen on
				pm = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
				wl = pm.newWakeLock(PowerManager.SCREEN_BRIGHT_WAKE_LOCK | PowerManager.ACQUIRE_CAUSES_WAKEUP, "myapp:mywakelocktag");
				if (!pm.isScreenOn()) {
					wl.acquire();
					mFinishHandler.postDelayed(finishRunnable, DEFAULT_SHOWING_TIME);
				}
			}
			// execute push noti listener
			if (pms.getOnReceivePushListener() != null) {
				if (pms.getOnReceivePushListener().onReceive(context, extras)) {
					showNotification(context, extras);
				}
			} else {
				showNotification(context, extras);
			}
		}

		CLog.i("ALERT FLAG : " + mPrefs.getString(PREF_ALERT_FLAG));

	}

	/**
	 * show notification
	 *
	 * @param context
	 * @param extras
	 */
	private synchronized void showNotification (Context context, Bundle extras) {

		CLog.i("showNotification");
		if (isImagePush(extras)) {
			showNotificationImageStyle(context, extras);
		} else {
			showNotificationTextStyle(context, extras);
		}
	}

	/**
	 * show notification text style
	 *
	 * @param context
	 * @param extras
	 */
	private synchronized void showNotificationTextStyle (Context context, Bundle extras) {
		CLog.i("showNotificationTextStyle");
		// push info
		PushMsg pushMsg = new PushMsg(extras);

		// notification
		int iconId = PMSUtil.getIconId(context);
		int largeIconId = PMSUtil.getLargeIconId(context);


		String strNotiChannel = DataKeyUtil.getDBKey(context, DB_NOTI_CHANNEL_ID);
		if (StringUtil.isEmpty(strNotiChannel)) {
			strNotiChannel = "0";
		}

		NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
		NotificationCompat.Builder builder;

		if (PMSUtil.getTargetVersion(context) > Build.VERSION_CODES.N_MR1 &&
				Build.VERSION.SDK_INT > Build.VERSION_CODES.N_MR1) {

			strNotiChannel = createNotiChannel(context, notificationManager, strNotiChannel);
			builder = new NotificationCompat.Builder(context, strNotiChannel);
			builder.setNumber(1);
		}
		else
		{
			builder = new NotificationCompat.Builder(context);
//			builder.setNumber(PMSDB.getInstance(context).selectNewMsgCnt());
		}

		int notificationId = getNotificationId();
		if(TextUtils.isEmpty(PMSUtil.getNotificationClickActivityClass(context))){
			builder.setContentIntent(makePendingIntent(context, extras, notificationId));
		} else{
			PendingIntent pendingIntent = makePendingIntentForClickActivity(context, extras, notificationId);
			if(pendingIntent != null){
				builder.setContentIntent(pendingIntent);
			} else{
				builder.setContentIntent(makePendingIntent(context, extras, notificationId));
			}
		}
		builder.setAutoCancel(true);
		builder.setContentText(pushMsg.notiMsg);
		builder.setContentTitle(pushMsg.notiTitle);
		builder.setTicker(pushMsg.notiMsg);
		// setting lights color
		builder.setLights(Notification.FLAG_SHOW_LIGHTS, 1000, 2000);
		builder.setPriority(NotificationCompat.PRIORITY_MAX);

		int ver = Build.VERSION.SDK_INT;
		if (ver >= Build.VERSION_CODES.LOLLIPOP) {
			iconId = PMSUtil.getIconId(context);
			largeIconId = PMSUtil.getLargeIconId(context);
			try {
				builder.setColor(Color.parseColor(PMSUtil.getNotiBackColor(context)));
			} catch (Exception e) {
			}
		} else {
			iconId = PMSUtil.getLargeIconId(context);
		}

		// set small icon
		CLog.i("small icon :" + iconId);
		if (iconId > 0) {
			builder.setSmallIcon(iconId);
		}

		// set large icon
		CLog.i("large icon :" + largeIconId);
		if (largeIconId > 0) {
			builder.setLargeIcon(BitmapFactory.decodeResource(context.getResources(), largeIconId));
		}

		// setting ring mode
		int noti_mode = ((AudioManager) context.getSystemService(Context.AUDIO_SERVICE)).getRingerMode();

		// checek ring mode
		if (noti_mode == AudioManager.RINGER_MODE_NORMAL) {
			if (FLAG_Y.equals(mPrefs.getString(PREF_RING_FLAG)) || StringUtil.isEmpty(mPrefs.getString(PREF_RING_FLAG))) {
				try {
					int notiSound = mPrefs.getInt(PREF_NOTI_SOUND);
					CLog.d("notiSound : "+notiSound);
					if (notiSound > 0) {
						Uri uri = Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE + "://" + context.getPackageName() + "/" + notiSound);
						builder.setSound(uri);
					} else {
						Uri uri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
						builder.setSound(uri);
					}
				} catch (Exception e) {
					CLog.e(e.getMessage());
				}
			}
		}

		// check vibe mode
		if (FLAG_Y.equals(mPrefs.getString(PREF_VIBE_FLAG)) || StringUtil.isEmpty(mPrefs.getString(PREF_VIBE_FLAG))) {
			builder.setDefaults(Notification.DEFAULT_VIBRATE);
		}

		if (FLAG_Y.equals(mPrefs.getString(PREF_USE_BIGTEXT))) {
			builder.setStyle(new NotificationCompat.BigTextStyle()
					.setBigContentTitle(pushMsg.notiTitle).bigText(pushMsg.notiMsg));
		}

		// show notification
//		if(FLAG_N.equals(DataKeyUtil.getDBKey(context, DB_NOTIFICATION_STACKABLE)))
//		{
//			notificationManager.cancelAll();
//		}

		notificationManager.notify(notificationId, builder.build());
	}

	/**
	 * show notification image style
	 *
	 * @param context
	 * @param extras
	 */
	@SuppressLint("NewApi")
	private synchronized void showNotificationImageStyle (Context context, Bundle extras) {
		CLog.i("showNotificationImageStyle");
		// push info
		PushMsg pushMsg = new PushMsg(extras);

		String strNotiChannel = DataKeyUtil.getDBKey(context, DB_NOTI_CHANNEL_ID);
		if (StringUtil.isEmpty(strNotiChannel)) {
			strNotiChannel = "0";
		}

		NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
		NotificationCompat.Builder builder;

		if (PMSUtil.getTargetVersion(context) > Build.VERSION_CODES.N_MR1 &&
				Build.VERSION.SDK_INT > Build.VERSION_CODES.N_MR1) {

			strNotiChannel = createNotiChannel(context, notificationManager, strNotiChannel);
			builder = new NotificationCompat.Builder(context, strNotiChannel);
			builder.setNumber(1);
		}
		else {
			builder = new NotificationCompat.Builder(context);
//			builder.setNumber(PMSDB.getInstance(context).selectNewMsgCnt());
		}

		int notificationId = getNotificationId();
		if(TextUtils.isEmpty(PMSUtil.getNotificationClickActivityClass(context))){
			builder.setContentIntent(makePendingIntent(context, extras, notificationId));
		} else{
			PendingIntent pendingIntent = makePendingIntentForClickActivity(context, extras, notificationId);
			if(pendingIntent != null){
				builder.setContentIntent(pendingIntent);
			} else{
				builder.setContentIntent(makePendingIntent(context, extras, notificationId));
			}
		}
		builder.setAutoCancel(true);
		builder.setContentText(pushMsg.notiMsg);
		builder.setContentTitle(pushMsg.notiTitle);
		builder.setTicker(pushMsg.notiMsg);
		// setting lights color
		builder.setLights(0xFF00FF00, 1000, 2000);
		builder.setPriority(Notification.PRIORITY_MAX);

		// notification
		// int iconId = mPrefs.getInt(PREF_NOTI_ICON) > 0 ? mPrefs.getInt(PREF_NOTI_ICON) : 0;
		int iconId;
		int largeIconId = 0;

		int ver = Build.VERSION.SDK_INT;
		if (ver >= Build.VERSION_CODES.LOLLIPOP) {
			iconId = PMSUtil.getIconId(context);
			largeIconId = PMSUtil.getLargeIconId(context);
			builder.setColor(Color.parseColor(PMSUtil.getNotiBackColor(context)));
		} else {
			iconId = PMSUtil.getLargeIconId(context);
		}

		// set small icon
		CLog.i("small icon :" + iconId);
		if (iconId > 0) {
			builder.setSmallIcon(iconId);
		}

		// set large icon
		CLog.i("large icon :" + largeIconId);
		if (largeIconId > 0) {
			builder.setLargeIcon(BitmapFactory.decodeResource(context.getResources(), largeIconId));
		}

		// setting ring mode
		int noti_mode = ((AudioManager) context.getSystemService(Context.AUDIO_SERVICE)).getRingerMode();

		// checek ring mode
		if (noti_mode == AudioManager.RINGER_MODE_NORMAL) {
			if (FLAG_Y.equals(mPrefs.getString(PREF_RING_FLAG)) || StringUtil.isEmpty(mPrefs.getString(PREF_RING_FLAG))) {
				try
				{
					int notiSound = mPrefs.getInt(PREF_NOTI_SOUND);
					CLog.d("notiSound : "+notiSound);
					if (notiSound > 0) {
						Uri uri = Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE + "://" + context.getPackageName() + "/" + notiSound);
						builder.setSound(uri);
					} else {
						Uri uri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
						builder.setSound(uri);
					}
				} catch (Exception e) {
					CLog.e(e.getMessage());
				}
			}
		}

		// check vibe mode
		if (FLAG_Y.equals(mPrefs.getString(PREF_VIBE_FLAG)) || StringUtil.isEmpty(mPrefs.getString(PREF_VIBE_FLAG))) {
			builder.setDefaults(Notification.DEFAULT_VIBRATE);
		}

		if (mPushImage == null) {
			CLog.e("mPushImage is null");
		}

		// show notification
		builder.setStyle(new NotificationCompat.BigPictureStyle(builder).bigPicture(mPushImage).setBigContentTitle(pushMsg.notiTitle)
				.setSummaryText(pushMsg.notiMsg));

		// show notification
//		if(FLAG_N.equals(DataKeyUtil.getDBKey(context, DB_NOTIFICATION_STACKABLE)))
//		{
//			notificationManager.cancelAll();
//		}

		notificationManager.notify(notificationId, builder.build());
	}

	/**
	 * make pending intent
	 *
	 * @param context
	 * @param extras
	 * @return
	 */
	private PendingIntent makePendingIntent (Context context, Bundle extras, int requestCode) {
		// notification
		Intent innerIntent = null;
		String receiverClass = null;
		receiverClass = mPrefs.getString(PREF_NOTI_RECEIVER_CLASS);
		CLog.d("receiverClass : " + receiverClass);
		if (receiverClass != null) {
			try {
				Class<?> cls = Class.forName(receiverClass);
				innerIntent = new Intent(context, cls).putExtras(extras);
				String receiverAction = mPrefs.getString(PREF_NOTI_RECEIVER);
				receiverAction = receiverAction != null ? receiverAction : "com.pms.sdk.notification";
				innerIntent.setAction(receiverAction);
			} catch (ClassNotFoundException e) {
				//e.printStackTrace();
			}
		}
		if (innerIntent == null) {
			CLog.d("makePendingIntent innerIntent is null");
			receiverClass = mPrefs.getString(PREF_NOTI_RECEIVER);
			receiverClass = receiverClass != null ? receiverClass : "com.pms.sdk.notification";
			innerIntent = new Intent(receiverClass).putExtras(extras);
		}

		PendingIntent pendingIntent = null;

		if(PMSUtil.getTargetVersion(context) >= 31){
			pendingIntent = PendingIntent.getBroadcast(context, requestCode, innerIntent, PendingIntent.FLAG_UPDATE_CURRENT | PendingIntent.FLAG_IMMUTABLE);
		} else{
			pendingIntent = PendingIntent.getBroadcast(context, requestCode, innerIntent, PendingIntent.FLAG_UPDATE_CURRENT);
		}

		return pendingIntent;
	}

	private PendingIntent makePendingIntentForClickActivity(Context context, Bundle extras, int notificationId){
		String activityAction = PMSUtil.getNotificationClickActivityAction(context);
		String activityClass = PMSUtil.getNotificationClickActivityClass(context);
		if(TextUtils.isEmpty(activityClass)){
			return null;
		}
		Intent intent = null;
		try
		{
			Class<?> cls = Class.forName(activityClass);
			intent = new Intent(context, cls);

		} catch (ClassNotFoundException e) {
			CLog.i("cannot found ("+(activityClass)+")");
		}
		if(intent == null){
			return null;
		}
		intent.putExtras(extras);
		intent.setAction(activityAction);
		intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
		if (PMSUtil.getTargetVersion(context) >= 31) {
			return PendingIntent.getActivity(context, notificationId, intent, PendingIntent.FLAG_UPDATE_CURRENT | PendingIntent.FLAG_IMMUTABLE);
		} else{
			return PendingIntent.getActivity(context, notificationId, intent, PendingIntent.FLAG_UPDATE_CURRENT);
		}
	}

	/**
	 * show popup (activity)
	 *
	 * @param context
	 * @param extras
	 */
	@SuppressLint("DefaultLocale")
	@SuppressWarnings("static-access")
	private synchronized void showPopup (Context context, Bundle extras) {

		ActivityManager am = (ActivityManager) context.getSystemService(context.ACTIVITY_SERVICE);
		List<ActivityManager.RunningTaskInfo> taskInfo = am.getRunningTasks(10);
		String temp = taskInfo.get(0).topActivity.getPackageName();

		Class<?> pushPopupActivity = null;

		String pushPopupActivityName = mPrefs.getString(PREF_PUSH_POPUP_ACTIVITY);

		if (StringUtil.isEmpty(pushPopupActivityName)) {
			pushPopupActivityName = PMSUtil.getPushPopupActivity(context);
		}

		try {
			pushPopupActivity = Class.forName(pushPopupActivityName);
		} catch (ClassNotFoundException e) {
			CLog.e(e.getMessage());
			try {
				pushPopupActivity = Class.forName(PMSUtil.getPushPopupActivity(context));
			} catch (ClassNotFoundException e1) {
				e1.printStackTrace();
			}
		}
		CLog.i("pushPopupActivity :" + pushPopupActivityName);

		Intent pushIntent = new Intent(context, pushPopupActivity);
		pushIntent.setFlags(Intent.FLAG_ACTIVITY_MULTIPLE_TASK | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_NO_ANIMATION
		// | Intent.FLAG_ACTIVITY_CLEAR_TOP
				| Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_NO_HISTORY);
		pushIntent.putExtras(extras);

		CLog.e("TOP Activity : " + temp);
		if (PMSUtil.getPopupActivity(context)) {
			if ((temp.toLowerCase().indexOf("launcher") != -1) // 런처
					|| (temp.toLowerCase().indexOf("locker") != -1) // 락커
					|| temp.equals("com.google.android.googlequicksearchbox") // 넥서스 5
					|| temp.equals("com.cashslide") // 캐시 슬라이드
					|| temp.equals("com.kakao.home") // 카카오런처
					|| temp.equals(context.getPackageName())) {

				context.startActivity(pushIntent);
			}
		} else {
			context.startActivity(pushIntent);
		}
	}

	/**
	 * is image push
	 *
	 * @param extras
	 * @return
	 */
	private boolean isImagePush (Bundle extras) {
		try {
			if (!PhoneState.isNotificationNewStyle()) {
				throw new Exception("wrong os version");
			}
			String notiImg = extras.getString(KEY_NOTI_IMG);
			CLog.i("notiImg:" + notiImg);
			if (TextUtils.isEmpty(notiImg))
			{
				throw new Exception("no image type");
			}
			return true;
		} catch (Exception e) {
			CLog.e("isImagePush:" + e.getMessage());
			return false;
		}
	}

	private Intent setPutExtra (Intent intent, String message) {
		try {
			JSONObject msgObj = new JSONObject(message);
			if (msgObj.has(KEY_MSG_ID)) {
				intent.putExtra(KEY_MSG_ID, msgObj.getString(KEY_MSG_ID));
			}
			if (msgObj.has(KEY_NOTI_TITLE)) {
				intent.putExtra(KEY_NOTI_TITLE, msgObj.getString(KEY_NOTI_TITLE));
			}
			if (msgObj.has(KEY_MSG_TYPE)) {
				intent.putExtra(KEY_MSG_TYPE, msgObj.getString(KEY_MSG_TYPE));
			}
			if (msgObj.has(KEY_NOTI_MSG)) {
				intent.putExtra(KEY_NOTI_MSG, msgObj.getString(KEY_NOTI_MSG));
			}
			if (msgObj.has(KEY_MSG)) {
				intent.putExtra(KEY_MSG, msgObj.getString(KEY_MSG));
			}
			if (msgObj.has(KEY_SOUND)) {
				intent.putExtra(KEY_SOUND, msgObj.getString(KEY_SOUND));
			}
			if (msgObj.has(KEY_NOTI_IMG)) {
				intent.putExtra(KEY_NOTI_IMG, msgObj.getString(KEY_NOTI_IMG));
			}
			if (msgObj.has(KEY_DATA)) {
				intent.putExtra(KEY_DATA, msgObj.getString(KEY_DATA));
			}
			return intent;
		} catch (Exception e) {
			e.printStackTrace();
			return intent;
		}
	}

	/**
	 * finish runnable
	 */
	private final Runnable finishRunnable = new Runnable() {
		@Override
		public void run () {
			if (wl != null && wl.isHeld()) {
				wl.release();
			}
		}
	};

	/**
	 * show instant view (toast)
	 *
	 *
	 */
	// private void showInstantView(Context context, String msg) {
	// LayoutInflater mInflater = LayoutInflater.from(context);
	// View v = mInflater.inflate(R.layout.pms_push_instant_view, null);
	// LinearLayout layPushMsg = (LinearLayout) v.findViewById(R.id.pms_lay_push_msg);
	// TextView txtPushTitle = (TextView) v.findViewById(R.id.pms_txt_push_title);
	// TextView txtPushMsg = (TextView) v.findViewById(R.id.pms_txt_push_msg);
	//
	// txtPushMsg.setText(msg);
	//
	// String currentTheme = Prefs.getString(context, Consts.PREF_THEME);
	// if (currentTheme.equals(context.getString(R.string.pms_txt_green))) {
	// layPushMsg.setBackgroundColor(0xCCC1C95E);
	// txtPushTitle.setTextColor(0xFFC1DF7D);
	// } else if (currentTheme.equals(context.getString(R.string.pms_txt_pink))) {
	// layPushMsg.setBackgroundColor(0xCCF695CA);
	// txtPushTitle.setTextColor(0xFFFED6F0);
	// } else if (currentTheme.equals(context.getString(R.string.pms_txt_brown))) {
	// layPushMsg.setBackgroundColor(0xCCD09F76);
	// txtPushTitle.setTextColor(0xFFFDD9B9);
	// } else if (currentTheme.equals(context.getString(R.string.pms_txt_black))) {
	// layPushMsg.setBackgroundColor(0xCC5C5C5C);
	// txtPushTitle.setTextColor(0xFF949494);
	// } else {
	// layPushMsg.setBackgroundColor(0xCCC1C95E);
	// txtPushTitle.setTextColor(0xFFC1DF7D);
	// }
	//
	// txtPushMsg.setTextColor(0xFFFFFFFF);
	//
	// Toast toast = new Toast(context);
	// toast.setGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL, 0, 0);
	// toast.setDuration(Toast.LENGTH_SHORT);
	// toast.setView(v);
	// toast.show();
	// }
	public int getNotificationId () {
		int notificationId = (int)(System.currentTimeMillis() % Integer.MAX_VALUE);
		return notificationId;
	}

	@TargetApi(Build.VERSION_CODES.O)
	private String createNotiChannel(Context context, NotificationManager notificationManager, String strNotiChannel)
	{
		NotificationChannel notiChannel = notificationManager.getNotificationChannel(strNotiChannel);
		boolean isShowBadge = false;
		boolean isPlaySound;
		boolean isPlaySoundChanged = false;
		boolean isEnableVibe;
		boolean isShowBadgeOnChannel;
		boolean isPlaySoundOnChannel;
		boolean isEnableVibeOnChannel;

		try
		{
			if(IPMSConsts.FLAG_Y.equals((String) context.getPackageManager().getApplicationInfo(context.getPackageName(),
					PackageManager.GET_META_DATA).metaData.get(IPMSConsts.META_DATA_NOTI_O_BADGE)))
			{
				isShowBadge = true;
			}
			else
			{
				isShowBadge = false;
			}
		}
		catch (PackageManager.NameNotFoundException e)
		{
			CLog.e(e.getMessage());
		}

		if(IPMSConsts.FLAG_Y.equals(mPrefs.getString(PREF_RING_FLAG)))
		{
			isPlaySound = true;
		}
		else
		{
			isPlaySound = false;
		}
		if(IPMSConsts.FLAG_Y.equals(mPrefs.getString(PREF_VIBE_FLAG)))
		{
			isEnableVibe = true;
		}
		else
		{
			isEnableVibe = false;
		}

		try
		{
			CLog.d("AppSetting isShowBadge "+ (String) context.getPackageManager().getApplicationInfo(context.getPackageName(),
					PackageManager.GET_META_DATA).metaData.get(IPMSConsts.META_DATA_NOTI_O_BADGE)+
					" isPlaySound "+mPrefs.getString(PREF_RING_FLAG)+
					" isEnableVibe "+mPrefs.getString(PREF_VIBE_FLAG));
		}
		catch (Exception e)
		{
			CLog.e(e.getMessage());
		}

		if (notiChannel == null)
		{    //if notichannel is not initialized
			CLog.d("notification initialized");
			notiChannel = new NotificationChannel(strNotiChannel, PMSUtil.getApplicationName(context), NotificationManager.IMPORTANCE_HIGH);
			notiChannel.setShowBadge(isShowBadge);
			notiChannel.enableVibration(isEnableVibe);
			notiChannel.setLightColor(Notification.FLAG_SHOW_LIGHTS);
			notiChannel.setImportance(NotificationManager.IMPORTANCE_HIGH);
			notiChannel.setLockscreenVisibility(Notification.VISIBILITY_PUBLIC);
			if (isEnableVibe)
			{
				notiChannel.setVibrationPattern(new long[]{1000, 1000});
			}

			if (isPlaySound)
			{
				Uri uri;
				try
				{
					int notiSound = mPrefs.getInt(PREF_NOTI_SOUND);
					if (notiSound > 0)
					{
						uri = Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE + "://" + context.getPackageName() + "/" + notiSound);
						CLog.d("notiSound " + notiSound + " uri " + uri.toString());
					} else
					{
						//throw new Exception("default ringtone is set");
						uri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
					}
				}
				catch (Exception e)
				{
					uri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
					CLog.e(e.getMessage());
				}

				AudioAttributes audioAttributes = new AudioAttributes.Builder()
						.setUsage(AudioAttributes.USAGE_NOTIFICATION)
						.build();
				notiChannel.setSound(uri, audioAttributes);
				CLog.d("setChannelSound ring with initialize notichannel");
			} else
			{
				notiChannel.setSound(null, null);
				CLog.d("setChannelSound muted with initialize notichannel");
			}
			notificationManager.createNotificationChannel(notiChannel);
			return strNotiChannel;
		} else
		{
			CLog.d("notification is exist");
			//19년 6월 5일 업체 요청으로 인한 채널 재생성 로직 제거
			return strNotiChannel;
		}
	}
}
