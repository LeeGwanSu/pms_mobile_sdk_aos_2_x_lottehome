package com.pms.sdk.push;

import org.json.JSONArray;
import org.json.JSONObject;

import android.content.Context;
import android.webkit.JavascriptInterface;

import com.pms.sdk.IPMSConsts;
import com.pms.sdk.api.APIManager;
import com.pms.sdk.api.request.ClickMsg;
import com.pms.sdk.api.request.ReadMsg;
import com.pms.sdk.common.util.CLog;
import com.pms.sdk.common.util.DateUtil;
import com.pms.sdk.common.util.PMSUtil;
import com.pms.sdk.common.util.Prefs;

/**
 * PMSWebViewBridge rich click 관련 클래스
 * 
 * @author erzisk
 * @since 2013.11.26
 */
public class PMSWebViewBridge implements IPMSConsts {

	private final Context mContext;

	public PMSWebViewBridge(Context context) {
		this.mContext = context;
	}

	@JavascriptInterface
	public boolean isReplaceLink () {
		CLog.i("call isReplaceLink");
		return true;
	}

	@JavascriptInterface
	public void addRichClick (String msgId, String linkSeq, String pushMsgId) {
		CLog.i("call onRichClick");
		JSONObject richClick = new JSONObject();
		try {
			richClick.put("msgId", msgId);
			richClick.put("linkSeq", linkSeq);
			richClick.put("msgPushType", pushMsgId);
			richClick.put("workday", DateUtil.getNowDate());
		} catch (Exception e) {
			e.printStackTrace();
		}
		CLog.d(String.format("msgId:%s,linkSeq:%s,pushMsgId:%s", msgId, linkSeq, pushMsgId));

		PMSUtil.arrayToPrefs(mContext, PREF_CLICK_LIST, richClick);
		CLog.d("clickList:" + PMSUtil.arrayFromPrefs(mContext, PREF_CLICK_LIST));

		JSONArray reads = new JSONArray();
		reads.put(PMSUtil.getReadParam(msgId));

		new ReadMsg(mContext).request(reads, null);
		new ClickMsg(mContext).request(PMSUtil.arrayFromPrefs(mContext, PREF_CLICK_LIST), new APIManager.APICallback() {
			@Override
			public void response (String code, JSONObject json) {
				if (CODE_SUCCESS.equals(code)) {
					new Prefs(mContext).putString(PREF_CLICK_LIST, "");
				}
			}
		});
	}
}
