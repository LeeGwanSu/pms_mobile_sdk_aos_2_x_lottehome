package com.pms.sdk;

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.text.TextUtils;
import android.widget.TextView;

import com.pms.sdk.api.APIManager.APICallback;
import com.pms.sdk.api.request.DeviceCert;
import com.pms.sdk.bean.Msg;
import com.pms.sdk.bean.MsgGrp;
import com.pms.sdk.common.util.CLog;
import com.pms.sdk.common.util.DataKeyUtil;
import com.pms.sdk.common.util.PMSUtil;
import com.pms.sdk.common.util.PhoneState;
import com.pms.sdk.common.util.Prefs;
import com.pms.sdk.common.util.StringUtil;
import com.pms.sdk.db.PMSDB;
import com.pms.sdk.view.Badge;

import org.json.JSONObject;

/**
 * @since 2012.12.28
 * @author erzisk
 * @description pms (solution main class)
 * @version [2013.10.07] pushMsg.java 생성 - push 데이터 key 변경<br>
 *          [2013.10.11] android-query -> volley 로 library 변경<br>
 *          [2013.10.14] push notification 출력 변경<br>
 *          [2013.10.16 15:09] logout시에 저장된 cust_id삭제<br>
 *          [2013.10.21 10:30] api call시에, appUserId 또는 encKey가 없을 시 에러 추가<br>
 *          [2013.10.21 11:22] api call시에, paging이 존재하는 api(newMsg.m)일 경우, 가장 마지막 호출에서만 apiCallback이 이뤄지게끔 수정<br>
 *          [2013.10.21 18:43] push 데이터 key 변경 (IPMSConsts)<br>
 *          [2013.10.25 14:15] deviceCert시 token이 없는경우 'noToken'으로 넘김<br>
 *          [2013.10.29 17:03] deviceCert시 token이 "noToken"일 경우도 loop<br>
 *          [2013.10.30 11:26] push 수신 시 notImg exception 처리 추가<br>
 *          [2013.11.01 11:15] CLog 새로운 버전으로 적용 및 소스에서 TAG 삭제!!!!<br>
 *          [2013.11.06 15:02] deviceCert parameter에 sessCnt추가<br>
 *          [2013.11.06 16:07] logined_cust_id 추가, deviceCert 및 loginPms시에 cust_id와 logined_cust_id를 비교하여 db delete 결정<br>
 *          [2013.11.11 09:25] (lotte) uuid setting 추가<br>
 *          [2013.11.15 10:38] APIManager에서 error발생 시에도 apiCalback수행<br>
 *          [2013.11.19 15:36] push popup finish thread 추가<br>
 *          [2013.11.26 09:22] click 관련 추가<br>
 *          [2013.11.26 16:42] MQTT Client 변경<br>
 *          [2013.11.26 21:13] deviceCert시 read 및 click api 호출<br>
 *          [2013.11.27 17:51] MQTT Client 사용시 로그 저장 루틴 추가 <br>
 *          [2013.12.04 17:47] push 수신 시 해당 msgId가 존재하면 noti하지 않게 변경<br>
 *          [2013.12.05 15:35] push popup 유지 시간 설정 추가 PMS.setPushPopupShowingTime(int)<br>
 *          [2013.12.10 18:21] MQTT KeepAlive Time mata-data에서 설정하도록 수정함.<br>
 *          [2013.12.11 16:07] PMSDB.deleteExpireMsg 로직 변경<br>
 *          [2013.12.12 18:34] Private Flag 추가<br>
 *          [2013.12.16 10:33] PushReceiver.onMessage에 synchronized추가<br>
 *          [2013.12.17 13:14] MQTT Client Service 중복 실행 체크 & ConnectionChangeReceiver.java 추가함. <br>
 *          [2013.12.18 17:46] (lotte) LoginPms시에 CustId가 같으면 API Call 안하게 변경.<br>
 *          [2013.12.19 09:02] MQTT Client ReConnect Timing 시간 변경 & ConectionChangeReceiver 변경 <br>
 *          [2013.12.19 17:35] MQTT Client Keep Alive 시간 랜덤으로 적용 & DeviceCert시 PrivateFlag 루틴 수정함. <br>
 *          [2013.12.27 13:08] GCM regist 시에 GCM_PACKAGE 전송 <br>
 *          [2013.12.27 14:53] DeviceCert 시 private Flag 처리 <br>
 *          [2014.01.10 13:29] MQTT Client 버그 사항 수정 및 안정화 <br>
 *          [2014.01.22 20:47] File Log 저장시 용량체크 후 삭제 <br>
 *          [2014.01.23 12:24] (lotte) DeviceCert시에 msgFlag, notiFlag를 업데이트 하지 않게 수정.<br>
 *          [2014.02.04 15:14] MQTT Client 파워 메니저 부분 수정.<br>
 *          [2014.02.05 16:36] 야간 모드 적용 & 로그 저장 루틴 추가<br>
 *          [2014.02.07 17:33] CollectLog 적용 부분 수정. <br>
 *          [2014.03.13 14:15] MQTT KeepAlive 부분 수정.<br>
 *          [2014.04.11 10:03] Notification 이벤트 적용.<br>
 *          [2014.06.09 11:11] 다른앱 실행시 팝업창 미노출 & Flag 추가함. <br>
 *          [2014.06.25 09:22] 예외처리 추가 & 팝업창 미노출 값 추가함. <br>
 *          [2014.07.30 10:17] Notification bar 우선순위 적용. <br>
 *          [2014.07.30 15:34] Notification bar 이벤트 삭제. <br>
 *          [2014.08.08 14:56] MQTT 버그 수정함. <br>
 *          [2014.08.21 11:45] API 호출시 마다 Cache Clear <br>
 *          [2014.09.11 11:00] Notification Icon을 블러오는 위치를 Androidmanifest.xml 쪽으로 바꿈. <br>
 *          [2014.10.22 11:15] Android OS 5.0에서 상태창 이미지 표시 안되는 버그 수정 & MQTT Client 최신버전으로 교체함.<br>
 *          [2015.03.18 17:25] 팝업창 안뜨게 수정함.<br>
 *          [2015.06.29 16:34] 로그파일 저장 안되게 수정함.<br>
 *          [2015.07.31 09:53] NewMsg Polling방식으로 가능하게 수정함.<br>
 *          [2015.07.31 17:31] GCM 사용 여부API로 설정함.<br>
 *          [2015.08.11 17:18] Polling 떄 OOM 발생 버그 수정함. Polling용 전용 API를 만듬.<br>
 *          [2015.10.26 10:28] mktFlag 추가함. LotteUUID 추가함. Android 6.0관련 내용 추가함.<br>
 *          [2015.10.28 10:51] LotteUUID -> imallUUID로 변경함.<br>
 *          [2015.10.28 13:52] mktFlag 삭제함.<br>
 *          [2016.04.29 09:33] X509TrustManager 코드 삭제하고 대응 코드 삽입함.<br>
 *          [2016.05.12 18:09] Push 수신시 noti_flag값 Check 하는 부분 삭제함.<br>
 *          [2016.07.05 09:43] WIFI Lock에서 Exception 발생하는 경우 안나도록 수정함.<br>
 *          [2016.07.05 16:40] LoginPms 호출시 NullPointException이 나는 현상 수정함.<br>
 *          [2016.12.02 18:36] doze mode broadcast 패치1 적용 <br>
 *          [2016.12.14 13:21] Icon에 대한 버그 사항 수정함.<br>
 *          [2017.02.14 16:10] 정보통신망법 관련하여 전화권한에 따른 수신거부 기능 개선 (DeviceCert, Notification) PhoneState Permission 체크<br>
 *          [2017.03.08 17:16] Notification BigTextStyle 기능 추가(캐리지 리턴적용되는 Notifcation)<br>
 *          [2017.03.15 17:23] PendingIntent NullPointerException 발생 문제<br>
 *          [2017.04.07 13:00] Notification PictureStyle 5.0, 6.0에서는 개행처리안되게 처리<br>
 *          [2017.05.17 11:11] Notification BigTextStyle setSummaryText -> setBig<br>
 *          [2018.01.11 16:34] :Release FCM 적용 (AndroidManifest / Gradle 수정)
 *          					8.0 대응 job service / scheduler 추가, 8.0 이상에서 job service로 동작하도록 수정 <br>
 *          [2018.01.15 16:54] JobSchedule시 setMinimumLatency를 설정해 놓지 않아 Doze 모드에서 정상적스로 수신되지 않는 문제 수정
 *          					(Empty push 메세지 올 경우 바로 깨어나서 Push 수신.
 *          					JobPushService 에서 기존 Wakelock 삭제, JobScheduler 에서는 Wake 관리를 하기 때문에 Wakelock 불필요<br>
 *          [2018.01.15 17:26] JobService 사용하길 원치 않아 JobService 동작하지 않도록 수정.
 *          					ServiceProvider 에서 IS_JOBSERVICE_ON 분기 두어 IS_JOBSERVICE_ON 이 true일 경우에만 Jobservice 로직 확인.
 *          					(유선상 통화로 잡서비스 사용하는 SDK 사용, 8.0 이상에서 동작함을 설명함)
 *          [2018.02.28 17:51] DB에서 cursor가 close 시 null체크 후 close 하도록 수정
 *          					Android 8.0 대응 수정, PushReceiver 에서 target과 디바이스 버전이 8.0 이상일 경우 푸시 메세지 Notify되지 않는 문제 수정
 *          					디바이스가 Android 8.0일 경우 푸시 선택시 동작하지 않는 문제 수정
 *          [2018.03.08 17:18] Android 4.4 버전 에서 AlarmPingSender 선언시 강제종료 발생하는 문제 수정
 *          [2018.03.28 16:45] 내부에 DB nullPointException 발생 수정
 *          [2018.10.15 09:35] Android 8.0 대응, 벨소리 채널 대응, 암시적 Intent 대응, 이미지 캐시 대응, MQTT Wakeuptime 적용
 *			[2018.10.15 12:57] 방해금지 모드 추가, notiback color 추가
 *			[2018.10.17 17:31] MQTT 오류 대응
 *		 	[2018.10.19 14:42] MQTT 안정화
 *		    [2018.10.19 16:54] MQTT 안정화 icon 에러 수정
 *		    [2018.10.23 09:54] MQTT 수정
 *		    [2018.11.06 14:53] MQTT 수정
 *		    [2018.11.08 15:17] 벨소리 수정
 *		    [2018.11.15 12:45] 벨소리 수정
 *		    [2018.11.27 14:35] 알림 소리 관련 수정
 *		    [2018.12.04 16:18] startforeground > alarmManager 수정
 *		    [2018.12.04 16:32] 알림 소리 변경 되지 않는 부분 수정
 *		    [2018.12.05 11:19] 생성된 알림채널이 없을 경우 에러 수정
 *		    [2019.01.28 17:00] MQTT 최신사항 반영(Binder, keepAliveTime 등)
 *		    [2019.02.18 11:27] FCM, instance 방식 >> onNewToke 방식으로 변경
 *		    [2019.02.19 13:34] onNewToke 누락으로 추가
 *		    [2019.04.12 15:53] * FCMInstanId Deprecated 대응
 *		                       * Token 발급 로직 변경 (deviceCert 비동기 처리 >> TMS 초기화 시점, FCM Service 등 추가 보안책 추가)
 *          [2019.04.12 17:10] 벨소리 원복기능 제거
 *			[2019.06.10 13:04] 업체 요청에 따른 벨소리 채널 재생성 로직 제거, NotificationImageStyle에서 NotificationCompat 쓰도록 변경
 *			[2019.06.20 16:20] PushReceiver에 액션 비교안해서 빈값 알림 표시되는 현상 수정
 *			[2019.11.20 14:05] 알림 쌓일수 있게 설정하는거 추가, Czip close 대응, makePendingIntent requestCode 대응, badge 사용처 제거, Android 10 대응
 */
	public class PMS implements IPMSConsts {

	// auth status
	public enum AUTH {
		PENDING, PROGRESS, FAIL, COMPLETE;
	}

	private AUTH authStatus = AUTH.PENDING;

	private static PMS instance;
	private Context mContext;

	private final PMSDB mDB;
	private final Prefs mPrefs;

	private OnReceivePushListener onReceivePushListener;

	/**
	 * constructor
	 * 
	 * @param context
	 */
	private PMS(final Context context) {
		this.mDB = PMSDB.getInstance(context);
		this.mPrefs = new Prefs(context);
		CLog.i("Version:" + PMS_VERSION + ",UpdateDate:201911201405");

		initOption(context);

		/**
		 * refresh db thread
		 */
//		new Thread(new Runnable() {
//			@Override
//			public void run () {
//				CLog.i("refreshDBHandler:start");
//				mDB.deleteExpireMsg();
//				mDB.deleteEmptyMsgGrp();
//				if (mContext != null) {
//					mContext.sendBroadcast(new Intent(context, Badge.class).setAction(RECEIVER_REQUERY));
//				}
//				CLog.i("refreshDBHandler:end");
//			}
//		}).start();
	}

	private void setmContext (Context context) {
		this.mContext = context;
	}

	private void unregisterReceiver () {
		Badge.getInstance(mContext).unregisterReceiver();
	}

	/**
	 * initialize option
	 */
	private void initOption (Context context) {
		if (StringUtil.isEmpty(mPrefs.getString(PREF_NOTI_FLAG))) {
			mPrefs.putString(PREF_NOTI_FLAG, "Y");
		}
		if (StringUtil.isEmpty(mPrefs.getString(PREF_MSG_FLAG))) {
			mPrefs.putString(PREF_MSG_FLAG, "Y");
		}
		if (StringUtil.isEmpty(mPrefs.getString(PREF_RING_FLAG))) {
			mPrefs.putString(PREF_RING_FLAG, "Y");
		}
		if (StringUtil.isEmpty(mPrefs.getString(PREF_VIBE_FLAG))) {
			mPrefs.putString(PREF_VIBE_FLAG, "Y");
		}
		if (StringUtil.isEmpty(mPrefs.getString(PREF_ALERT_FLAG))) {
			mPrefs.putString(PREF_ALERT_FLAG, "Y");
		}
		if (StringUtil.isEmpty(mPrefs.getString(PREF_MAX_USER_MSG_ID))) {
			mPrefs.putString(PREF_MAX_USER_MSG_ID, "-1");
		}
		if (StringUtil.isEmpty(mPrefs.getString(PREF_SCREEN_WAKEUP_FLAG))) {
			mPrefs.putString(PREF_SCREEN_WAKEUP_FLAG, "Y");
		}
		if (StringUtil.isEmpty(PMSUtil.getServerUrl(context))) {
			PMSUtil.setServerUrl(context, API_SERVER_URL);
		}
		if (StringUtil.isEmpty(mPrefs.getString(PREF_POPUP_CANCEL_TEXT))) {
			mPrefs.putString(PREF_POPUP_CANCEL_TEXT, "CLOSE");
		}
		if (StringUtil.isEmpty(mPrefs.getString(PREF_POPUP_CONFIRM_TEXT))) {
			mPrefs.putString(PREF_POPUP_CONFIRM_TEXT, "OK");
		}
		if (StringUtil.isEmpty(mPrefs.getString(PREF_PUSH_POPUP_ACTIVITY))) {
			mPrefs.putString(PREF_PUSH_POPUP_ACTIVITY, DEFAULT_PUSH_POPUP_ACTIVITY);
		}
		if (mPrefs.getInt(PREF_PUSH_POPUP_SHOWING_TIME) < 1) {
			mPrefs.putInt(PREF_PUSH_POPUP_SHOWING_TIME, 10000);
		}
	}

	/**
	 * getInstance
	 *
	 * @param context
	 * @return
	 */
	public static PMS getInstance (Context context)
	{
		return getInstance(context, "");
	}

	/**
	 * getInstance
	 * 
	 * @param context
	 * @param projectId
	 * @return
	 */
	public static PMS getInstance (final Context context, String projectId)
	{
		if (instance == null)
		{
			instance = new PMS(context);
		}

		//
		instance.setmContext(context);

		if (!TextUtils.isEmpty(projectId))
		{
			PMSUtil.setGCMProjectId(context, projectId);
			CLog.d("getInstance:projectId=" + PMSUtil.getGCMProjectId(context));
		}
		return instance;
	}

	/**
	 * clear
	 * 
	 * @return
	 */
	public static boolean clear () {
		try {
			PMSUtil.setDeviceCertStatus(instance.mContext, DEVICECERT_PENDING);
			instance.unregisterReceiver();
			instance = null;
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	/**
	 * cust id 세팅
	 * 
	 * @param custId
	 */
	public void setCustId (String custId) {
		CLog.i("setCustId");
		CLog.d("setCustId:custId=" + custId);
		PMSUtil.setCustId(mContext, custId);
	}

	/**
	 * get cust id
	 * 
	 * @return
	 */
	public String getCustId () {
		CLog.i("getCustId");
		return PMSUtil.getCustId(mContext);
	}

	/**
	 * set uuid
	 * 
	 * @param uuid
	 */
	public void setUuid (String uuid) {
		CLog.i("setUuid");
		CLog.d("setUuid:uuid=" + uuid);
		mPrefs.putString(PREF_UUID, uuid);
	}

	/**
	 * get Uuid
	 */
	public void getUuid () {
		CLog.i("getUuid");
		mPrefs.getString(PREF_UUID);
	}

	public void setImallUuid (String imalluuid) {
		CLog.i("setImallUuid");
		CLog.d("setImallUuid:uuid=" + imalluuid);
		mPrefs.putString(PREF_LOTTE_UUID, imalluuid);
	}

	public void isGCMPush (Boolean isgcm) {
		PMSUtil.setIsGCMPush(mContext, isgcm);
	}

	public void setIsPopupActivity (Boolean ispopup) {
		PMSUtil.setPopupActivity(mContext, ispopup);
	}

	/**
	 * set debugTag
	 * 
	 * @param tagName
	 */
	public void setDebugTAG (String tagName) {
		CLog.setTagName(tagName);
	}

	/**
	 * set debug mode
	 * 
	 * @param debugMode
	 */
	public void setDebugMode (boolean debugMode) {
		CLog.setDebugMode(debugMode);
	}

	public void setOnReceivePushListener (OnReceivePushListener onReceivePushListener) {
		this.onReceivePushListener = onReceivePushListener;
	}

	public OnReceivePushListener getOnReceivePushListener () {
		return onReceivePushListener;
	}

	/**
	 * get msg flag
	 * 
	 * @return
	 */
	public String getMsgFlag () {
		return mPrefs.getString(PREF_MSG_FLAG);
	}

	/**
	 * get noti flag
	 * 
	 * @return
	 */
	public String getNotiFlag () {
		return mPrefs.getString(PREF_NOTI_FLAG);
	}

	/**
	 * set large noti icon
	 * 
	 * @param resId
	 */
	public void setLargeNotiIcon (int resId) {
		mPrefs.putInt(PREF_LARGE_NOTI_ICON, resId);
	}

	/**
	 * set noti cound
	 * 
	 * @param resId
	 */
	public void setNotiSound (int resId) {
		mPrefs.putInt(PREF_NOTI_SOUND, resId);
	}

	/**
	 * set noti receiver
	 * 
	 * @param intentAction
	 */
	public void setNotiReceiver (String intentAction) {
		mPrefs.putString(PREF_NOTI_RECEIVER, intentAction);
	}

	/**
	 * set noti receiver class
	 *
	 * @param intentClass
	 */
	public void setNotiReceiverClass (String intentClass) {
		mPrefs.putString(PREF_NOTI_RECEIVER_CLASS, intentClass);
	}


	/**
	 * set ring mode
	 * 
	 * @param isRingMode
	 */
	public void setRingMode (boolean isRingMode) {
		mPrefs.putString(PREF_RING_FLAG, isRingMode ? "Y" : "N");
	}

	/**
	 * set vibe mode
	 * 
	 * @param isVibeMode
	 */
	public void setVibeMode (boolean isVibeMode) {
		mPrefs.putString(PREF_VIBE_FLAG, isVibeMode ? "Y" : "N");
	}

	/**
	 * set popup noti
	 * 
	 * @param isShowPopup
	 */
	public void setPopupNoti (boolean isShowPopup) {
		mPrefs.putString(PREF_ALERT_FLAG, isShowPopup ? "Y" : "N");
	}

	/**
	 * set screen wakeup
	 * 
	 * @param isScreenWakeup
	 */
	public void setScreenWakeup (boolean isScreenWakeup) {
		mPrefs.putString(PREF_SCREEN_WAKEUP_FLAG, isScreenWakeup ? "Y" : "N");
	}

	public void setUseBigText (Boolean enable) {
		mPrefs.putString(PREF_USE_BIGTEXT, enable ? "Y" : "N");
	}

	public void setPushToken(String token){
		PMSUtil.setGCMToken(mContext, token);
	}


	/**
	 * set push popup activity
	 * 
	 * @param classPath
	 */
	public void setPushPopupActivity (String classPath) {
		mPrefs.putString(PREF_PUSH_POPUP_ACTIVITY, classPath);
	}

	/**
	 * set pushPopup showing time
	 * 
	 * @param pushPopupShowingTime
	 */
	public void setPushPopupShowingTime (int pushPopupShowingTime) {
		mPrefs.putInt(PREF_PUSH_POPUP_SHOWING_TIME, pushPopupShowingTime);
	}

	public void bindBadge (Context c, int id) {
//		Badge.getInstance(c).addBadge(c, id);
	}

	public void bindBadge (TextView badge)
	{
//		Badge.getInstance(badge.getContext()).addBadge(badge);
	}

	/**
	 * show Message Box
	 * 
	 * @param c
	 */
	public void showMsgBox (Context c) {
		showMsgBox(c, null);
	}

	public void showMsgBox (Context c, Bundle extras) {
		CLog.i("showMsgBox");
		if (PhoneState.isAvailablePush()) {
			if (authStatus == AUTH.PENDING || authStatus == AUTH.FAIL) {
				// auth가 유효한 상태가 아니라면 authrize진행
				CLog.i("showMsgBox:no authorized");
				authorize();
			}

			try {
				Class<?> inboxActivity = Class.forName(mPrefs.getString(PREF_INBOX_ACTIVITY));

				Intent i = new Intent(mContext, inboxActivity);
				i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
				if (extras != null) {
					i.putExtras(extras);
				}
				c.startActivity(i);
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			}

			// Intent i = new Intent(INBOX_ACTIVITY);
			// i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
			// if (extras != null) {
			// i.putExtras(extras);
			// }
			// c.startActivity(i);
		}
	}

	public void closeMsgBox (Context c) {
//		Intent i = new Intent(c, Badge.class).setAction(RECEIVER_CLOSE_INBOX);
//		c.sendBroadcast(i);
	}

	/**
	 * pms device 인증
	 * 
	 * @return
	 */
	public void authorize () {
		CLog.i("authorize:start");
		// auth status setting
		authStatus = AUTH.PROGRESS;

		new DeviceCert(mContext).request(null, new APICallback() {
			@Override
			public void response (String code, JSONObject json) {

				if (CODE_SUCCESS.equals(code)) {
					// success device cert
					CLog.i("DeviceCert:success device cert");
					authStatus = AUTH.COMPLETE;
				} else {
					// fail device cert
					CLog.i("DeviceCert:fail device cert");
					authStatus = AUTH.FAIL;
				}
			}
		});
	}

	/*
	 * ===================================================== [start] database =====================================================
	 */
	/**
	 * select MsgGrp list
	 * 
	 * @return
	 */
	public Cursor selectMsgGrpList () {
		return mDB.selectMsgGrpList();
	}

	/**
	 * select MsgGrp
	 * 
	 * @param msgCode
	 * @return
	 */
	public MsgGrp selectMsGrp (String msgCode) {
		return mDB.selectMsgGrp(msgCode);
	}

	/**
	 * select new msg Cnt
	 * 
	 * @return
	 */
	public int selectNewMsgCnt ()
	{
//		return mDB.selectNewMsgCnt();
		return mDB.getAllOfUnreadMsgCount();
	}

	/**
	 * select Msg List
	 * 
	 * @param page
	 * @param row
	 * @return
	 */
	public Cursor selectMsgList (int page, int row) {
		return mDB.selectMsgList(page, row);
	}

	/**
	 * select Msg List
	 * 
	 * @param msgCode
	 * @return
	 */
	public Cursor selectMsgList (String msgCode) {
		return mDB.selectMsgList(msgCode);
	}

	/**
	 * select Msg
	 * 
	 * @param userMsgId
	 * @return
	 */
	public Msg selectMsg (String userMsgId) {
		return mDB.selectMsg(userMsgId);
	}

	/**
	 * select Msg
	 * 
	 * @param msgId
	 * @return
	 */
	public Msg selectMsgWhereMsgId (String msgId) {
		return mDB.selectMsgWhereMsgId(msgId);
	}

	/**
	 * update MsgGrp
	 * 
	 * @param msgCode
	 * @param values
	 * @return
	 */
	public long updateMsgGrp (String msgCode, ContentValues values) {
		return mDB.updateMsgGrp(msgCode, values);
	}

	/**
	 * update read msg
	 * 
	 * @param msgGrpCd
	 * @param firstUserMsgId
	 * @param lastUserMsgId
	 * @return
	 */
	public long updateReadMsg (String msgGrpCd, String firstUserMsgId, String lastUserMsgId) {
		return mDB.updateReadMsg(msgGrpCd, firstUserMsgId, lastUserMsgId);
	}

	/**
	 * update read msg where userMsgId
	 * 
	 * @param userMsgId
	 * @return
	 */
	public long updateReadMsgWhereUserMsgId (String userMsgId) {
		return mDB.updateReadMsgWhereUserMsgId(userMsgId);
	}

	/**
	 * update read msg where msgId
	 * 
	 * @param msgId
	 * @return
	 */
	public long updateReadMsgWhereMsgId (String msgId) {
		return mDB.updateReadMsgWhereMsgId(msgId);
	}

	/**
	 * delete Msg
	 * 
	 * @param userMsgId
	 * @return
	 */
	public long deleteMsg (String userMsgId) {
		return mDB.deleteMsg(userMsgId);
	}

	/**
	 * delete MsgGrp
	 * 
	 * @param msgCode
	 * @return
	 */
	public long deleteMsgGrp (String msgCode) {
		return mDB.deleteMsgGrp(msgCode);
	}

	/**
	 * delete expire Msg
	 * 
	 * @return
	 */
	public long deleteExpireMsg () {
		return mDB.deleteExpireMsg();
	}

	/**
	 * delete empty MsgGrp
	 * 
	 * @return
	 */
	public void deleteEmptyMsgGrp () {
		mDB.deleteEmptyMsgGrp();
	}

	/**
	 * delete all
	 */
	public void deleteAll () {
		mDB.deleteAll();
	}

	/*
	 * ===================================================== [end] database =====================================================
	 */
	public void setPushReceiverClass (String intentClass) {
		mPrefs.putString(PREF_PUSH_RECEIVER_CLASS, intentClass);
	}

	public void setNotificationStackable(boolean isEnable)
	{
		DataKeyUtil.setDBKey(mContext, DB_NOTIFICATION_STACKABLE, isEnable?FLAG_Y:FLAG_N);
	}

	public void setDoNotDisturb(Context context, boolean isEnable)
	{
		DataKeyUtil.setDBKey(context, DB_DO_NOT_DISTURB_FLAG, isEnable?FLAG_Y:FLAG_N);
	}
	public void setDoNotDisturbTime(Context context, String fromTimeFormatLikeHHMM, String toTimeFormatLikeHHMM)
	{
		StringBuilder builder = new StringBuilder();
		builder.append(fromTimeFormatLikeHHMM);
		builder.append(toTimeFormatLikeHHMM);
		DataKeyUtil.setDBKey(context, DB_DO_NOT_DISTURB_TIME, builder.toString());
	}

	public void setPushClickActivity(String action, String className){
		PMSUtil.setNotificationClickActivityClass(mContext, className);
		PMSUtil.setNotificationClickActivityAction(mContext, action);
	}

	public void createNotificationChannel(){
		PMSUtil.createNotificationChannel(mContext);
	}
}
